package unittests

//import unittests.TestCase
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import org.apache.spark.rdd.{RDD}
import eu.ferari.spark.prm.core.radonpoint.RadonPoint
import eu.ferari.spark.prm.core.learner.{Learner, AverageZeroOneLoss}
import scala.math.abs
import scala.runtime.ScalaRunTime.stringOf
import scala.reflect._
import eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper._
import org.apache.spark.mllib.classification.{SVMWithSGD, SVMModel, LogisticRegressionWithSGD, LogisticRegressionModel, LogisticRegressionWithLBFGS}
import weka.classifiers.functions.Logistic
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.experiments.datautils.HdfsDataUtil

object TestHdfsDataUtil extends TestCase {    
  
  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  } 
  
  def run(sqlContext: SQLContext): Unit = { 
    /* Read dummy test data */
    
    val data = HdfsDataUtil.getDataset("20_newsgroups.drift", sqlContext)//sqlContext.read.format("libsvm").load(getClass.getResource("").getPath()+"../../../../src/main/resources/prm/data/classification/dummyDataBinClass_libsvm.txt")
    data.show()
  }  
}
