package unittests

//import unittests.TestCase
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext
import eu.ferari.spark.prm.core.radonpoint.RadonPoint
import scala.math.abs
import scala.runtime.ScalaRunTime.stringOf
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.junit.Test

class TestRadonPointCalculation extends TestCase{
  
  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  } 
  
  @Test def run{
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
    
    sc.stop()
  }
  
  def run(sqlContext: SQLContext): Unit = { 
    /* Test computation of single Radon point */
    
    print("Testing Radon point calculation...")
    val epsilon = 0.000001
    
    val radonNumber = 4
    var S = new Array[Array[Double]](0)
    S = S :+ Array(1.0,1.0)
    S = S :+ Array(3.0,1.0)
    S = S :+ Array(1.0,3.0)
    S = S :+ Array(3.0,3.0)    
    var r = RadonPoint.getRadonPoint(S, sqlContext)
    //println("S = " + stringOf(S))
    //println("r = " + stringOf(r))
    var value = abs(r(0) + r(1) - 4.0)
    assert(value < epsilon, "Radon point not as expected. \nExpected is (2.0,2.0) but calculated was " + stringOf(r) + ".\n The difference is "+stringOf(value)+" which is larger than the allowed difference of "+stringOf(epsilon)+".")
    print("succesful.\n")
    
    /* Test iterated Radon point computation for height 2*/
        
    var h = 2    
    print("Testing iterated Radon point calculation with height h="+stringOf(h)+"...")
    
    S = new Array[Array[Double]](0)    
    S = S :+ Array(1.0,1.0)
    S = S :+ Array(3.0,1.0)
    S = S :+ Array(1.0,3.0)
    S = S :+ Array(3.0,3.0)
    
    S = S :+ Array(4.0,4.0)
    S = S :+ Array(4.0,6.0)
    S = S :+ Array(6.0,4.0)
    S = S :+ Array(6.0,6.0)
    
    S = S :+ Array(4.0,1.0)
    S = S :+ Array(4.0,3.0)
    S = S :+ Array(6.0,3.0)
    S = S :+ Array(6.0,1.0)
    
    S = S :+ Array(1.0,4.0)
    S = S :+ Array(1.0,6.0)
    S = S :+ Array(3.0,4.0)
    S = S :+ Array(3.0,6.0)
    
    r = RadonPoint.getIteratedRadonPoint(S, h, radonNumber, sqlContext)
    value = abs(r(0) + r(1) - 7.0)
    assert(value < epsilon, "Iterated Radon point (h=2) not as expected. \nExpected is (3.5,3.5) but calculated was " + stringOf(r) + ".\n The difference is "+stringOf(value)+" which is larger than the allowed difference of "+stringOf(epsilon)+".")
    print("successful.\n")
    
    /* Test iterated Radon point computation for height 1*/
    
    h = 1
    print("Testing iterated Radon point calculation with height h="+stringOf(h)+"...")
    
    S = new Array[Array[Double]](0)    
    S = S :+ Array(1.0,1.0)
    S = S :+ Array(3.0,1.0)
    S = S :+ Array(1.0,3.0)
    S = S :+ Array(3.0,3.0)
    
    S = S :+ Array(4.0,4.0)
    S = S :+ Array(4.0,6.0)
    S = S :+ Array(6.0,4.0)
    S = S :+ Array(6.0,6.0)
    
    S = S :+ Array(4.0,1.0)
    S = S :+ Array(4.0,3.0)
    S = S :+ Array(6.0,3.0)
    S = S :+ Array(6.0,1.0)
    
    S = S :+ Array(1.0,4.0)
    S = S :+ Array(1.0,6.0)
    S = S :+ Array(3.0,4.0)
    S = S :+ Array(3.0,6.0)
    
    // Infinitely many points with 1 <= x <= 6 and 1 <= y <= 6 are possible. However, the closer the point is to (3.5,3.5), the better. 
    // The SVD variant deterministically provides the point (3.65554765,3.89472417) as best solution in local mode, so this is the point against which we are testing.    
    r = RadonPoint.getIteratedRadonPoint(S, h, radonNumber, sqlContext)
    value = abs(r(0) - 3.65554765 + r(1) - 3.89472417)
    assert(value < epsilon, "Iterated Radon point (h=1) not as expected. \nExpected is (3.65554765,3.89472417) but calculated was " + stringOf(r) + ".\n The difference is "+stringOf(value)+" which is larger than the allowed difference of "+stringOf(epsilon)+".")
    print("successful.\n")
  }
}