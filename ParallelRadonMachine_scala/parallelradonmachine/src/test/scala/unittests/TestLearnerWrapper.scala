package unittests

//import unittests.TestCase
import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import org.apache.spark.rdd.{RDD}
import eu.ferari.spark.prm.core.radonpoint.RadonPoint
import eu.ferari.spark.prm.core.learner.{Learner, AverageZeroOneLoss}
import scala.math.abs
import scala.runtime.ScalaRunTime.stringOf
import scala.reflect._
import eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper._
import org.apache.spark.mllib.classification.{SVMWithSGD, SVMModel, LogisticRegressionWithSGD, LogisticRegressionModel, LogisticRegressionWithLBFGS}
import org.apache.spark.ml.classification.{MultilayerPerceptronClassifier}
import weka.classifiers.functions.{Logistic, LinearRegression, SGD, MultilayerPerceptron}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import eu.ferari.spark.prm.experiments.datautils.DataUtil
import org.junit.Test

object TestLearnerWrapper extends TestLearnerWrapper {
  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  } 
}

class TestLearnerWrapper extends TestCase{      
  
  @Test def run{
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
    
    sc.stop()
  }
  
  def run(sqlContext: SQLContext): Unit = { 
    /* Read dummy test data */
    
    val data = DataUtil.getDataset("dummyDataBinClass_libsvm", sqlContext)//sqlContext.read.format("libsvm").load(getClass.getResource("").getPath()+"../../../../src/main/resources/prm/data/classification/dummyDataBinClass_libsvm.txt")
    
    val splits = data.randomSplit(Array(0.7,0.3))
    val traindata = splits(0)
    val testdata = splits(1)
    
    /* Instantiate a learner and test it*/              
    
    var learner = new SparkMLLogisticRegression()
    var model = learner.fit(traindata)
    var preds = model.predict(testdata)
    var coefs = model.getParameters()
    
    var weightsSomewhatCorrect : Boolean = (coefs(0) > 0) & (coefs(1) > 0) & (coefs(2) < 0) & (coefs(3) >= 0) & (coefs(0) < coefs(1)) & (coefs(1) < abs(coefs(2)))
    assert(weightsSomewhatCorrect, "Error: Model coefficients are far off from correct. Expected coefficients are (0.3, 0.5, -0.7, 0.5) but learner gave "+coefs.mkString("(", ",", ")")+".")    
    //println(coefs.mkString("(", ",", ")"))
    
    /* Test learner factory */

    var opt = Learner(classTag[MultilayerPerceptron])//LogisticRegressionWithLBFGS])
    assert(opt.isDefined, "Error: Factory returned None for LogisticRegressionWithLBFGS.")    
    var newlearner = opt.get        
    var newmodel = newlearner.fit(traindata)    
    preds = newmodel.predict(testdata)
    coefs = newmodel.getParameters()
    
    weightsSomewhatCorrect = (coefs(0) > 0) & (coefs(1) > 0) & (coefs(2) < 0) & (coefs(3) >= 0) & (coefs(0) < coefs(1)) & (coefs(1) < abs(coefs(2)))
    //assert(weightsSomewhatCorrect, "Model coefficients are far off from correct. Expected coefficients are (0.3, 0.5, -0.7, 0.5) but learner gave "+coefs.mkString("(", ",", ")")+".")
    //println(coefs.mkString("(", ",", ")"))
    
    /* Test all wrappers */
    
    var learnerBaseClasses = Learner.getWrappedLearners()    
    learnerBaseClasses.map(l => testLearner(l, data, false))    
  }  
  
  def testLearner(baseLearnerClass: ClassTag[_], data: DataFrame, throwAssertions: Boolean = true) {
    val splits = data.randomSplit(Array(0.7,0.3))
    val traindata = splits(0)
    val testdata = splits(1)
    
    var coefs = Array(-10.0,-10.0,-10.0,-10.0)
    print("Testing "+stringifyClass(baseLearnerClass)+" init_wrapper=")
    try {
      var opt = Learner(baseLearnerClass)
      if (throwAssertions) {
        assert(opt.isDefined, "Error: Factory returned None for learner "+stringOf(baseLearnerClass)+".")
      }
      var newlearner = opt.get
      print("ok.     fit=")
      var newmodel = newlearner.fit(traindata)
      print("ok.     predict=")
      var preds = newmodel.predict(testdata)
//      if (baseLearnerClass.toString().contains("LinearRegression")) {
//        preds = preds.map(x => if (x <= 0.0)  0.0  else  1.0 )
//      }
      print("ok.     get_model_parameters=")
      coefs = newmodel.getParameters()
      print("ok.     prediction_rerror=")
      var trueVals = testdata.select("label").collect().map(x=>x.getAs[Double]("label")).toArray
      var error = AverageZeroOneLoss(trueVals, preds)
      var errorOk = "(ok)      "
      if (error > 0.4) {
        errorOk = "(failed)  "
      }
      println(f"$error%1.3f" + errorOk)
    } catch {
      case e: Exception => print("failed. ("+e.toString()+")")
    }
    
    var weightsSomewhatCorrect = (coefs(0) > 0) & (coefs(1) > 0) & (coefs(2) < 0) & (coefs(3) >= 0) & (coefs(1) < abs(coefs(2)))  | coefs.length > 4
    if (throwAssertions) {
      assert(weightsSomewhatCorrect, "Model coefficients are far off from correct. Expected coefficients are (0.3, 0.5, -0.7, 0.5) but learner gave "+coefs.mkString("(", ",", ")")+".")
    }
    
    if (weightsSomewhatCorrect) {
      println(" test = successful.")
    }
    else {
      println(" test = failed (weights not as expected).")
    }
  }
}