package unittests

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame, Row}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.junit.Test
import eu.ferari.spark.prm.core.learner.{AUC, ACC, AverageHingeLoss, AverageZeroOneLoss, HingeLoss, ZeroOneLoss, MRE, RMSE, RCD, MSE}

object TestMetrics extends TestMetrics{     
  def main(args: Array[String]): Unit = {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  } 
}

class TestMetrics extends TestCase{    
  var eps = 0.0000000000001 
  @Test def run() {
    System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
    
    val conf = new SparkConf().setAppName("Unit-Test Baselines").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
    
    sc.stop()
  }  
  
  def run(sqlContext: SQLContext): Unit = { 
    var trueVals = Array[Double](0,0,1,1,0,1,0,0,1,1,1)
    var predVals = Array(0.1,0.8,1.1,0.1,0.4,0.8,0.5,0.2,0.5,1.2,1.3)
    
    val auc = AUC(trueVals, predVals)
    assert(math.abs(auc - 0.783333333333333) <  eps, "Error: AUC is "+auc.toString()+" but should have been 0.7833333333333334")
    
    val acc = ACC(trueVals, predVals)
    assert(math.abs(acc - 0.545454545454545455) <  eps, "Error: ACC is "+acc.toString()+" but should have been 0.545454545455")

    val hinge = HingeLoss(trueVals, predVals)
    assert(math.abs(hinge - 8.6) <  eps, "Error: HingeLoss is "+hinge.toString()+" but should have been 8.6")
    
    val avghinge = AverageHingeLoss(trueVals, predVals)
    assert(math.abs(avghinge - 0.7818181818181817) <  eps, "Error: AverageHingeLoss is "+avghinge.toString()+" but should have been 0.7818181818181817")
    
    val zeroOne = ZeroOneLoss(trueVals, predVals)
    assert(math.abs(zeroOne - 5.0) <  eps, "Error: ZeroOneLoss is "+zeroOne.toString()+" but should have been 5.0")
    
    val avgZeroOne = AverageZeroOneLoss(trueVals, predVals)
    assert(math.abs(avgZeroOne - 0.45454545454545453) <  eps, "Error: AverageZeroOneLoss is "+avgZeroOne.toString()+" but should have been 0.45454545454545453")
    
    val mre = MRE(trueVals, predVals)
    assert(math.abs(mre - 0.38181818181818183) <  eps, "Error: MRE is "+mre.toString()+" but should have been 0.38181818181818183")
    
    val mse = MSE(trueVals, predVals)
    assert(math.abs(mse - 0.21272727272727276) <  eps, "Error: MSE is "+mse.toString()+" but should have been 0.21272727272727276")
    
    val rmse = RMSE(trueVals, predVals)
    assert(math.abs(rmse - 0.46122366887148447) <  eps, "Error: RMSE is "+rmse.toString()+" but should have been 0.46122366887148447")
    
    val rcd = RCD(trueVals, predVals)
    assert(math.abs(rcd - 0.6443949989404535) <  eps, "Error: RCD is "+rcd.toString()+" but should have been 0.6443949989404535")
  }
}