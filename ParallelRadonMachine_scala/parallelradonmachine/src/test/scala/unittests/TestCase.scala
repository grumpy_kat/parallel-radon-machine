package unittests

import scala.reflect._
import scala.runtime.ScalaRunTime.stringOf
import org.apache.spark.sql.SQLContext
import org.scalatest.FunSuite

abstract class TestCase extends FunSuite{
  def run(sqlContext: SQLContext): Unit
  
  def stringifyClass(clazz:ClassTag[_]) : String = {
    var name = stringOf(clazz)
    return name.padTo(70, " ").mkString
  }
}