package eu.ferari.spark.prm.experiments.mlj.classification

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, DataFrame}
import org.apache.log4j.Logger
import org.apache.log4j.Level
import scala.reflect._
import eu.ferari.spark.prm.core.learner._
import eu.ferari.spark.prm.core.learner.baselines.AveragingAtTheEnd
import eu.ferari.spark.prm.core.ParallelRadonMachine
import eu.ferari.spark.prm.experiments.{Experiment, ExperimentDefinition}
import eu.ferari.spark.prm.experiments.datautils.{DataUtil, HdfsDataUtil}
import org.apache.spark.ml.classification.{MultilayerPerceptronClassifier}
import weka.classifiers.functions.{MultilayerPerceptron}

object RadonOnMLPs extends ExperimentDefinition{
  var name = "MLJ_Classification_RadonOnMLPs"
  var clusterMode = false 
  var expBasePath = "/home/IAIS/mkamp/scala/PRM/experiments/"
  
  def run(sqlContext: SQLContext): Unit = {   
    var datasets = HdfsDataUtil.datasets("classification").keys.toArray
    //datasets = datasets.filterNot { p => p.contains("skin_segmentation") }
    
    datasets.foreach{ dataset =>
      //WEKA MLP implementation and SPARK MLP implementation
      var mlps = Array[Learner[Model]]( Learner(classTag[MultilayerPerceptron]).get.asInstanceOf[Learner[Model]], 
                                        Learner(classTag[MultilayerPerceptronClassifier]).get.asInstanceOf[Learner[Model]])    
      var radonMachines = mlps.map{l => 
          {
            var prm = new ParallelRadonMachine(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> -1, "minSampleSize" -> 100))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var radonMachinesh1 = mlps.map{l => 
          {
            var prm = new ParallelRadonMachine(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> 1, "minSampleSize" -> 100))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var avgs = mlps.map{l => 
          {
            var prm = new AveragingAtTheEnd(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> -1, "minSampleSize" -> 100))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var avg1s = mlps.map{l => 
          {
            var prm = new AveragingAtTheEnd(l)
            prm.setConfig(scala.collection.mutable.Map[String, Any]("h" -> 1, "minSampleSize" -> 100))
            prm.asInstanceOf[Learner[Model]]
          }
        }.toArray
      var learners = mlps ++ radonMachines ++ radonMachinesh1 ++ avgs ++ avg1s
      var metrics = Array(AverageZeroOneLoss, AverageHingeLoss, ACC, AUC)
      
      
      val nFolds = 10
      val exp = new Experiment(name+"_"+dataset.replace(".dat",""), clusterMode, expBasePath)
      exp.run(sqlContext, Array(dataset), learners, metrics, nFolds)
    }
  }
}