package eu.ferari.spark.prm.experiments.datautils

import weka.core.converters.{ArffLoader, LibSVMSaver}
import java.io.File

object ArffToLibSVMConverter {
  def main(args: Array[String]): Unit = {
    
    for (taskType <- DataUtil.taskTypes) {
      for ((key, value) <- DataUtil.datasets(taskType)) {
        if (value.contains(".arff") & !value.contains("CASP")) {
          println(key)
          convert(value)
        }
      }
    }
  }
  
  def convert(inFile: String): Unit = {
    var loader = new ArffLoader()
    loader.setSource(new File(inFile))
    var data = loader.getDataSet()
    var libsvmSaver = new LibSVMSaver()
    libsvmSaver.setInstances(data)
    libsvmSaver.setFile(new File(inFile.replace(".arff",".dat")));
		libsvmSaver.writeBatch();
  }
}