package eu.ferari.spark.prm.core.learner

import org.apache.spark.sql.{Dataset, Row}

abstract class Model extends Serializable{
  var parameters = Array[Double]()
  def setParameters(parameters: Array[Double])
  def getParameters(): Array[Double]  
  def predict(data: Dataset[Row]) : Array[Double]
}

/* A dummy model object */
class EmpyModel extends Model {
  def setParameters(parameters: Array[Double]) {
    this.parameters = parameters
  }
  
  def getParameters(): Array[Double]  = {
    return parameters
  }
  
  def predict(data: Dataset[Row]) : Array[Double] = {
    return Array.fill(data.count().toInt)(0.0)
  }
}