package eu.ferari.spark.prm.core

import eu.ferari.spark.prm.core.learner.Learner
import org.apache.spark.SparkContext
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.sql.{Dataset, Row}
import eu.ferari.spark.prm.core.radonpoint.RadonPoint
import eu.ferari.spark.prm.core.learner.{Model, EmpyModel}
import eu.ferari.spark.prm.core.learner.wrapper.{SerialLearner, SparkLearner}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.types.{DoubleType, StructType, StructField}
import org.apache.spark.sql.Row
import collection.mutable.HashMap
import eu.ferari.spark.prm.core.Utils.{getStratifiedSample, getStratifiedKeys, dfZipWithIndex}
import scala.collection.parallel.mutable.ParArray
import scala.collection.mutable.MutableList
import org.apache.spark.sql.Column
import org.apache.spark.HashPartitioner

/**
 * @author Michael Kamp, Fraunhofer IAIS and University of Bonn, michael.kamp@iais.fraunhofer.de
 * Implementation of the Parallel Radon Machine (Michael Kamp, Mario Boley, and Thomas Gärtner. Parallelizing Randomized Convex Optimization. OPT workshop at NIPS 2016)
 * @baseLearner: instance of the learner that is to be parallelized. Wrappers for learning algorithms can be found in the package core.learner.wrapper
 * @stName: identifier of this instance used in the results and log files 
 * @h Number of Radon iterations. Together with Radon number of the model glass r, this defines the number of parallel instances of the learner, i.e., c = r^h. h < 0 means "take the maximum h so that each parallel learner instance gets at least minSampleSize data samples".
 * @minSampleSize Minimum number of data instances each parallel learner receives. If h is specified such that each learner would receive less samples, then h is reduced.
 * @minSampleSizeForRadonNumberCalculation In order to get the Radon number of the model class, the learner is trained on a small sample of the data. The number of model parameters d then determines the Radon number r = d+2. 
 * @useMapPartition Parallelization of learners can be performed in two ways: (i) MapPartition: repartition the data into c partitions and then execute the learners fit-function within a mapPartition on the dataset (this is usually 10-times faster than the split approach); (ii) Split: split the dataset into c different Dataset[Row]-objects and store them in a ParArray. Then call the fit method in a parallel loop on each dataset. Since the datasets are not necessarily perfectly parallelized, this might require some centralization of data.
 * @return returns a Model instance of the same type that the baseLearner would return.
 */
class ParallelRadonMachine(val baseLearner: Learner[_ <: Model], val stName: String = "") extends Learner  {
  var model: Model = new EmpyModel()
  var durations = HashMap[String, Long] ("DataSplit" -> 0, "ModelFit" -> 0, "RadonCalc" -> 0, "FitTotal" -> 0)  
  protected val config = scala.collection.mutable.Map[String, Any]("h" -> 1, "minSampleSizeForRadonNumberCalculation" -> 100, "useMapPartition" -> true, "minSampleSize" -> 10)
  var currentC = -1 
  var currentH = config("h").asInstanceOf[Int]
  var currentR = 0
  if (baseLearner.isInstanceOf[SparkLearner[_]]) {
    config("useMapPartition") = false
  }
//  else if (baseLearner.isInstanceOf[SerialLearner[_]]) {
//    config("useMapPartition") = true
//  }  
  
  def fit(data: Dataset[Row]): Model = {
    val h = config("h").asInstanceOf[Int]
    return fit(data, h, false)
  }
  
  def predict(data: Dataset[Row]): Dataset[Row] = {    
    val sqlContext = data.sqlContext
    val predArray = model.predict(data)    
    val predictions = sqlContext.sparkContext.parallelize(predArray).map(x => Row(x))
    val predDF = sqlContext.createDataFrame(predictions, StructType(Array(StructField("prediction", DoubleType, nullable=false))))        
    return predDF
  }
  
  def fit(data: Dataset[Row], stratifiedSample: Boolean): Model = {
    val h = config("h").asInstanceOf[Int]
    return fit(data, h, stratifiedSample)
  }
  
  def fit(data: Dataset[Row], h: Int, stratifiedSample: Boolean = true, randomSeed: Option[Long] = None) : Model = {    
    val useMapPartition = config("useMapPartition").asInstanceOf[Boolean]
    
    val sqlContext = data.sqlContext
    val startFit = System.nanoTime
    val N = data.count()  
    val minSampleSizeForRadonNumberCalculation = config("minSampleSizeForRadonNumberCalculation").asInstanceOf[Int]
    val r = RadonPoint.getRadonNumber(data, baseLearner, minSampleSizeForRadonNumberCalculation.toFloat / N.toFloat)
    currentR = r
    println("Radon number: "+r.toString+" ")
    var c = getC(h, r, N.toInt)        
    var durationSplit: Long   = 0
    var durationFitOnly: Long = 0
    var S: Array[Array[Double]] = Array(Array(0.0))
    
    if (useMapPartition) {
      //repartition the data on the spark workers, then run mappartition
      //is truly parallel on the worker nodes
      val startSplit = System.nanoTime
      val repartitionedData = repartitionDataWithKey(data, c, stratifiedSample, randomSeed)
      val endSplit = System.nanoTime
      durationSplit = endSplit - startSplit
      //initialize the model with 100 data points            
      var fraction = if (minSampleSizeForRadonNumberCalculation < N) minSampleSizeForRadonNumberCalculation.toFloat/N.toFloat else 0.5
      var sample = data.sample(false, fraction).cache()
      model = baseLearner.fit(sample)
      val startFitOnly = System.nanoTime
      
      //repartitionedData.rdd.mapPartitions(iter => Array(iter.size).iterator, true).collect().foreach(println) //some debug output to check that the data has been correctly repartitioned
      S = trainModelsOnRepartitionedAndKeyedData(repartitionedData, List.range(0,c))
      val endFitOnly = System.nanoTime    
      durationFitOnly = endFitOnly - startFitOnly
    }
    else {
      //split the data into several Dataset[Row]s and store them in an array
      //is only parallel on the spark driver node
      val startSplit = System.nanoTime
      val splittedData = splitDataIntoSeparateDataset(data, c, stratifiedSample, randomSeed)    
      val endSplit = System.nanoTime
      durationSplit = endSplit - startSplit
      //initialize the model
      model = baseLearner.fit(splittedData(0))
      //train on each Dataset[Row] using scala map to obtain model parameters
      val startFitOnly = System.nanoTime
      S = trainModelsOnSplittedData(splittedData)
      val endFitOnly = System.nanoTime    
      durationFitOnly = endFitOnly - startFitOnly      
    }
    
    val startRadon = System.nanoTime    
    var parameters = RadonPoint.getIteratedRadonPoint(S, h, r, sqlContext)    
    val endRadon = System.nanoTime    
    val durationRadon = endRadon - startRadon
    
    model.setParameters(parameters)    
    
    val endFit = System.nanoTime
    val durationFit = endFit - startFit
    
    durations = HashMap("DataSplit" -> durationSplit, "ModelFit" -> durationFitOnly, "RadonCalc" -> durationRadon, "FitTotal" -> durationFit, "c" -> c)
    return model
  }    
  
  def trainModelsOnSplittedData(splittedData: ParArray[Dataset[Row]]): Array[Array[Double]] = {                   
    var S = splittedData.map { case subset => {
        //val baseLearner = baseLearnerClass.newInstance()
        baseLearner.fit(subset).getParameters()
      }      
    }
    return S.toArray
  }    
  
  def trainModelsOnRepartitionedAndKeyedData(data: Dataset[Row], keys: List[Int]): Array[Array[Double]] = {    
    import data.sparkSession.implicits._
    var parKeys = keys.par
    if (baseLearner.isInstanceOf[SerialLearner[_]]) {
      val labelIdx = data.columns.indexOf("label")
      var S = data.drop("key").mapPartitions(baseLearner.asInstanceOf[SerialLearner[_]].applyFit(labelIdx)).collect()
      return S
    }
    else {
      var S = parKeys.map { k =>
        baseLearner.fit(data.filter(data("key") === k).drop("key")).getParameters()      
      }
      return S.toArray
    }
  } 
  
  override def toString(): String = {
    var h = config("h").asInstanceOf[Int]
    return "ParallelRadonMachine(h="+ (if (h > 0)  h.toString() else  "max") +")["+baseLearner.toString()+"]" + (if (stName.length() > 0) "_" else "") + stName
  }
  
  //ensures that each split has exactly one partition. This way, the number of splits and partitions is equal and 
  //a learner executed on one split will produce exactly one hypothesis.
  def splitDataIntoSeparateDataset(data: Dataset[Row], c: Int, stratifiedSample: Boolean, randomSeed: Option[Long]): ParArray[Dataset[Row]] = {    
    var datasets = if (!stratifiedSample) data.randomSplit(Array.fill(c)(1), randomSeed.getOrElse(System.nanoTime())).par else getStratifiedSample(data, c, randomSeed).par    
    return datasets.map{d => d.repartition(1)} 
  }
  
  def getNewModelInstance(data: Option[Dataset[Row]] = None): Model = {
    return baseLearner.getNewModelInstance(data)
  }
  
  def repartitionDataWithKey(data: Dataset[Row], c: Int, stratifiedSample: Boolean, randomSeed: Option[Long]): Dataset[Row] = {    
    import data.sqlContext.implicits._
    
    var keyedData = data
    if (!stratifiedSample) {   
      val N = data.count().toInt
      keyedData = dfZipWithIndex(data, 0, "key", false, c)
    }
    else {
      keyedData = getStratifiedKeys(data, c, randomSeed)
    }
    //keyedData.select("key").rdd.countByValue().foreach(println) //just some debug output to check that partitions are nearly equal in size
    var repartitionedKeyedData = keyedData.repartition($"key")
    //VERY IMPORTANT. Otherwise, the lazy evaluation screws up:
    repartitionedKeyedData.rdd.mapPartitions(iter => Array(iter.size).iterator, true).collect()
    repartitionedKeyedData = data.sqlContext.createDataFrame(repartitionedKeyedData.rdd.coalesce(c, false), keyedData.schema)
    repartitionedKeyedData.collect() //not sure if this is necessary here, but just to be sure...
//    println("+++")
//    repartitionedKeyedData.rdd.mapPartitions(iter => Array(iter.size).iterator, true).collect()
//    println("+++")
    return repartitionedKeyedData
  }
  
  def getC(h: Int, r: Int, N: Int): Int = {
    val minSampleSize = config("minSampleSize").asInstanceOf[Int]
    if (h >= 0) { //that is, a specific h has been explicitly defined
      var c = math.pow(r, h).toInt
      var newh = h
      while (N / c < minSampleSize && newh > 0) {
        newh -= 1
        c = math.pow(r, newh).toInt
      }
      currentH = newh
      currentC = c
      return c
    }
    else { //h == -1 means, chose the maximum possible h
      var largestC = (N / minSampleSize).floor.toInt
      var largestH = (math.log(largestC) / math.log(r)).floor.toInt
      var c = math.pow(r, largestH).toInt
      currentC = c
      currentH = largestH
      return c
    }
  }
  
  override def getConfigString(): String = {
    var strOut = ""
    config.foreach { case(key, value) => strOut += key + "=" + value.toString() + ";" }
    strOut += "c="+currentC.toString()+";"
    strOut += "actH="+currentH.toString()+";"
    strOut += "r="+currentR.toString()+";"
    strOut += "["+this.baseLearner.toString()+":"+this.baseLearner.getConfigString()+"]"
    return strOut
  }
}