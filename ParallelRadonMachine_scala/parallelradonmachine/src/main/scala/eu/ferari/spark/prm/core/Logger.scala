package eu.ferari.spark.prm.core

import org.apache.log4j.{Logger => Log4JLogger}

object Logger extends Serializable {
  @transient lazy val log = Log4JLogger.getLogger(getClass.getName)
}