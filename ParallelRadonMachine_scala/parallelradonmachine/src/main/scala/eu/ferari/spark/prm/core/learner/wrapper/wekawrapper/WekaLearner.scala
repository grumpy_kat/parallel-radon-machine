package eu.ferari.spark.prm.core.learner.wrapper.wekawrapper

import org.apache.spark.sql.{Dataset, Row}
import scala.reflect._
import eu.ferari.spark.prm.core.learner.{Learner, Model}
import eu.ferari.spark.prm.core.learner.wrapper.SerialLearner
import eu.ferari.spark.prm.prmannotations.WrapsLearner
import weka.classifiers.functions.{Logistic, LinearRegression, SGD, MultilayerPerceptron}
import weka.core.{Instances, Instance, DenseInstance, Attribute}
import java.util.ArrayList;

abstract class WekaLearner[M <: WekaModel[CLASSIFIER] : ClassTag, CLASSIFIER] extends SerialLearner[M] {
  var minTrainForGetNewModelInstance = 100.0
  
  def fit(data: List[Row], labelIdx: Int): Array[Double] = {
    var model  : M = classTag[M].runtimeClass.newInstance.asInstanceOf[M]//= modelClass.newInstance()      
    val isRegression = (this.getClass.toString().contains("regression") | this.getClass.toString().contains("Regression")) & !(this.getClass.toString().contains("logistic") | this.getClass.toString().contains("Logistic"))    
    model.setModel(train(listOfRowsToInstances(data, labelIdx, isRegression)))
    return model.getParameters()
  }
  
  def getNewModelInstance(data: Option[Dataset[Row]] = None): M = {
    var model  : M = classTag[M].runtimeClass.newInstance.asInstanceOf[M]
    if (!data.isEmpty) {
      val isRegression = (this.getClass.toString().contains("regression") | this.getClass.toString().contains("Regression")) & !(this.getClass.toString().contains("logistic") | this.getClass.toString().contains("Logistic"))
      val labelIdx = data.get.columns.indexOf("label")
      val N = data.get.count().toFloat
      var fraction = if (minTrainForGetNewModelInstance < N) minTrainForGetNewModelInstance/N else 0.9
      var dataSample = data.get.sample(false, fraction)
      model.setModel(train(listOfRowsToInstances(dataSample.collect.toList, labelIdx, isRegression)))
    }
    return model
  }
  
  def train(instances: Instances): CLASSIFIER
  
  def listOfRowsToInstances(data: List[Row], labelIdx: Int, isRegression: Boolean = false): Instances = {
    var attributes = new ArrayList[Attribute]()
    var labelAtt = new Attribute("label")
    if (!isRegression) { //if the task is classification or multi-class, the label attribute has to be nominal
      val labels = data.map{row => row.get(labelIdx).asInstanceOf[Double]}.distinct.toArray.map(x => x.toString)
      val labelsArrayList: ArrayList[String] = new ArrayList()
      labels.map(x => labelsArrayList.add(x))    
      labelAtt = new Attribute("label", labelsArrayList)
    }
    attributes.add(labelAtt)
    var exampleInstance = data(0)
    var features = exampleInstance.getAs[org.apache.spark.ml.linalg.Vector]("features").toArray
    for (i <- List.range(0, features.length)) {
      attributes.add(new Attribute("x"+i.toString()))
    }    
    var instances = new Instances("data", attributes, data.length)
    instances.setClass(labelAtt)
    val numAtts = instances.numAttributes()
    data.map(row => addRow(row, instances, numAtts))
    return instances
  }
  
  def addRow(row: Row, instances: Instances, numAtts: Int): Unit = {
    var label = row.getAs[Double]("label");   
    var feats = row.getAs[org.apache.spark.ml.linalg.Vector]("features").toArray;                     
    var vals = new Array[Double](numAtts)
    vals(0) = label
    for (i <- List.range(0, feats.length)) {
      vals(i+1) = feats(i)
    }
    var inst = new DenseInstance(1.0, vals)
    instances.add(inst)
  }
}

@WrapsLearner(name = classOf[Logistic])
class WekaLogisticRegression extends WekaLearner[WekaLogisticModel, Logistic] {
  protected val config = scala.collection.mutable.Map[String, Any]("-R" -> 1e-8, "-M" -> -1)
  
  def train(instances: Instances): Logistic = {
    var logClassifier = new Logistic()
    val options = config.toArray.map(x => Array[String](x._1, x._2.toString)).flatten
    //println(options)
    logClassifier.setOptions(options)
    logClassifier.buildClassifier(instances)
    return logClassifier
  }
}

@WrapsLearner(name = classOf[LinearRegression])
class WekaLinearRegression extends WekaLearner[WekaLinearRegressionModel, LinearRegression] {
  protected val config = scala.collection.mutable.Map[String, Any]("-R" -> 1e-8, "-S" -> 0)
  
  def train(instances: Instances): LinearRegression = {
    var linReg = new LinearRegression()
    val options = config.toArray.map(x => Array[String](x._1, x._2.toString)).flatten
    linReg.setOptions(options)
    linReg.buildClassifier(instances)
    return linReg
  }
}

@WrapsLearner(name = classOf[SGD])
class WekaSGD extends WekaLearner[WekaSGDModel, SGD] {
  protected val config = scala.collection.mutable.Map[String, Any]("-F" -> 0, "-L" -> 0.01, "-R" -> 0.0001, "-E" -> 500)
  
  def train(instances: Instances): SGD = {
    var sgdClassifier = new SGD()
    val options = config.toArray.map(x => Array[String](x._1, x._2.toString)).flatten
    sgdClassifier.setOptions(options)
    sgdClassifier.buildClassifier(instances)
    return sgdClassifier
  }
}

@WrapsLearner(name = classOf[MultilayerPerceptron])
class WekaMLP extends WekaLearner[WekaMLPModel, MultilayerPerceptron] {
  protected val config = scala.collection.mutable.Map[String, Any]("-H" -> "4,4", "-L" -> 0.3, "-E" -> 500, "-M" -> 0.2, "-N" -> 1000)
  
  def train(instances: Instances): MultilayerPerceptron = {
    var mlpClassifier = new MultilayerPerceptron()
    val options = config.toArray.map(x => Array[String](x._1, x._2.toString)).flatten
    mlpClassifier.setOptions(options)
    mlpClassifier.buildClassifier(instances)
    return mlpClassifier
  }
}
