package eu.ferari.spark.prm.experiments

import java.io.{File, BufferedWriter, FileWriter}
import eu.ferari.spark.prm.core.learner.{Learner, Model, Metric}
import eu.ferari.spark.prm.core.ParallelRadonMachine
import eu.ferari.spark.prm.core.learner.baselines.AveragingAtTheEnd

class ExperimentLogger(outFolder: String) {
  val logDirectory = outFolder + "logs/"
  val resultsDirectory = outFolder + "results/"
  val configsDirectory = outFolder + "configs/"
  createDirectoryStructure()
  
  def createDirectoryStructure(): Unit = {
    var baseFolder = new File(outFolder.substring(0, outFolder.substring(0, outFolder.length()-1).lastIndexOf("/")+1))
    if (!baseFolder.exists()) {
      baseFolder.mkdir()
    }
    new File(outFolder).mkdir()
    for (dir <- List(logDirectory, resultsDirectory, configsDirectory)) {
      new File(dir).mkdir()
    }
  }
  
  def writeResults(fitTime: Long, predTime: Long, errors: Array[Double], metrics: Array[String], learner: Learner[_], datasetName: String, fold: Int): Unit = {
    val filename = (datasetName.replace(".dat","") + "_" + learner.toString() + ".log").replace(":", "")
    var outFile = new File(resultsDirectory + filename)
    if (!outFile.exists()) {
      outFile.createNewFile()
      var headline = "Fold, Training Time, Prediction Time, "
      for (m <- metrics) {
        headline += m.toString() + ", "
      }
      headline += "Timing Details\n\n"
      writeToFile(outFile, headline)
    }
    var strOut = fold.toString() + ", " + fitTime.toString() + ", " + predTime.toString() + ", "
    for (e <- errors) {
      strOut += e.toString() + ", "
    }
    strOut += getTimingDetails(learner) + "\n"
    writeToFile(outFile, strOut)
  }
  
  def writePredictionLog(trueVals: Array[Double], predVals: Array[Double], datasetName: String, learner: Learner[_], fold: Int): Unit = {
    val filename = (datasetName.replace(".dat","") + "_" + learner.toString() + ".log").replace(":", "")
    var outFile = new File(logDirectory + filename)
    if (!outFile.exists()) {
      outFile.createNewFile()
    }
    var strOut: String = "Fold " + fold.toString() + "\n\n"
    for (i <- List.range(0, trueVals.length)) {
      strOut += trueVals(i).toString() + ", " + predVals(i) + "\n" 
    }
    strOut += "\n"
    writeToFile(outFile, strOut)
  }
  
  def writeLearnerConfig(learner: Learner[_], fold: Int, datasetName: String): Unit = {
    val filename = (learner.toString() + ".log").replace(":", "")
    var outFile = new File(configsDirectory + filename)
    if (!outFile.exists()) {
      outFile.createNewFile()
      var headline = "Fold, Dataset, Config\n\n"
      writeToFile(outFile, headline)
    }
    var stOut = fold.toString() + "," + datasetName + "," + learner.getConfigString() + "\n"
    writeToFile(outFile, stOut)
  }
  
  def getTimingDetails(learner: Learner[_]): String = {
    if (learner.getClass.toString().contains("ParallelRadonMachine")) {
      return learner.asInstanceOf[ParallelRadonMachine].durations.map(pair => pair._1+"="+pair._2).mkString("{",", ","}")
    }
    else if (learner.getClass.toString().contains("AveragingAtTheEnd")) {
      return learner.asInstanceOf[AveragingAtTheEnd].durations.map(pair => pair._1+"="+pair._2).mkString("{",", ","}")
    }
    else {
      return "None"
    }
  }
  
  def writeToFile(file: File, content: String, bAppend: Boolean = true) {
    var output = new BufferedWriter(new FileWriter(file, bAppend));
    output.write(content);
    output.close()
  }
}