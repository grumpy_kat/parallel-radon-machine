package eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper

import scala.reflect._
import eu.ferari.spark.prm.core.learner.{Learner, Model}
import eu.ferari.spark.prm.core.learner.wrapper.SparkLearner
import eu.ferari.spark.prm.prmannotations.WrapsLearner
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.{Vector, SparseVector, Vectors}
import org.apache.spark.mllib.classification.{SVMWithSGD, SVMModel, LogisticRegressionWithSGD, LogisticRegressionModel, LogisticRegressionWithLBFGS}
import org.apache.spark.mllib.regression.{LabeledPoint, LinearRegressionWithSGD, LinearRegressionModel, LassoWithSGD, LassoModel, RidgeRegressionWithSGD, RidgeRegressionModel, GeneralizedLinearModel, GeneralizedLinearAlgorithm}

/* Spark MLLib Learners (RDD-Interface)*/
 
abstract class SparkMLLibLearner[M <: SparkMLLibLinearModel[SPARKMODEL] : ClassTag, SPARKMODEL <: GeneralizedLinearModel] 
    extends SparkLearner[M] {  
  
  def fit(data: Dataset[Row]) : M = {
    import data.sqlContext.implicits._
    val rddData = data.map(row => LabeledPoint(
         row.getAs[Double]("label"),   
         Vectors.dense(row.getAs[org.apache.spark.ml.linalg.SparseVector]("features").toArray)
      ))
    return train(rddData.rdd)
  }
  
  def train(data: RDD[LabeledPoint]): M = {
    var model  : M = getNewModelInstance()
    model.setModel(trainSparkModel(data))
    return model
  }
  
  def trainSparkModel(data: RDD[LabeledPoint]): SPARKMODEL
}

@WrapsLearner(name = classOf[SVMWithSGD])
class SparkMLLibSVMWithSGD
    extends SparkMLLibLearner[SparkMLLibSVMModel, SVMModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "stepSize" -> 1.0, "regParam" -> 0.01, "miniBatchFraction" -> 1.0)

  val modelClass = (new SparkMLLibSVMModel()).getClass
  def trainSparkModel(data: RDD[LabeledPoint]): SVMModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val stepSize = config("stepSize").asInstanceOf[Double]
    val regParam = config("regParam").asInstanceOf[Double]
    val miniBatchFraction = config("miniBatchFraction").asInstanceOf[Double]
    return SVMWithSGD.train(data, iterations, stepSize, regParam, miniBatchFraction)
  }
}

@WrapsLearner(name = classOf[LogisticRegressionWithSGD])
class SparkMLLibLogisticRegressionWithSGD
    extends SparkMLLibLearner[SparkMLLibLogisticRegressionModel, LogisticRegressionModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "stepSize" -> 1.0, "miniBatchFraction" -> 1.0)
  
  val modelClass = (new SparkMLLibLogisticRegressionModel()).getClass
  def trainSparkModel(data: RDD[LabeledPoint]): LogisticRegressionModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val stepSize  = config("stepSize").asInstanceOf[Double]
    val miniBatchFraction  = config("miniBatchFraction").asInstanceOf[Double]
    return LogisticRegressionWithSGD.train(data, iterations, stepSize, miniBatchFraction)
  }
}

@WrapsLearner(name = classOf[LogisticRegressionWithLBFGS])
class SparkMLLibLogisticRegressionWithLBFGS
    extends SparkMLLibLearner[SparkMLLibLogisticRegressionModel, LogisticRegressionModel] {
  protected val config = scala.collection.mutable.Map[String, Any]()
  
  val modelClass = (new SparkMLLibLogisticRegressionModel()).getClass
  def trainSparkModel(data: RDD[LabeledPoint]): LogisticRegressionModel = {
    return new LogisticRegressionWithLBFGS().setNumClasses(2).run(data)
  }
}

@WrapsLearner(name = classOf[LinearRegressionWithSGD])
class SparkMLLibLinearRegressionWithSGD
    extends SparkMLLibLearner[SparkMLLibLinearRegressionModel, LinearRegressionModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "stepSize" -> 1.0, "miniBatchFraction" -> 1.0)

  val modelClass = (new SparkMLLibLinearRegressionModel()).getClass
  def trainSparkModel(data: RDD[LabeledPoint]): LinearRegressionModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val stepSize  = config("stepSize").asInstanceOf[Double]
    val miniBatchFraction  = config("miniBatchFraction").asInstanceOf[Double]
    return LinearRegressionWithSGD.train(data, iterations, stepSize, miniBatchFraction)
  }
}

@WrapsLearner(name = classOf[LassoWithSGD])
class SparkMLLibLassoWithSGD
    extends SparkMLLibLearner[SparkMLLibLassoModel, LassoModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "stepSize" -> 1.0, "regParam" -> 0.01, "miniBatchFraction" -> 1.0)
  
  val modelClass = (new SparkMLLibLassoModel()).getClass
  def trainSparkModel(data: RDD[LabeledPoint]): LassoModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val stepSize = config("stepSize").asInstanceOf[Double]
    val regParam = config("regParam").asInstanceOf[Double]
    val miniBatchFraction = config("miniBatchFraction").asInstanceOf[Double]
    return LassoWithSGD.train(data, iterations, stepSize, regParam, miniBatchFraction)
  }
}

@WrapsLearner(name = classOf[RidgeRegressionWithSGD])
class SparkMLLibRidgeRegressionWithSGD
    extends SparkMLLibLearner[SparkMLLibRidgeRegressionModel, RidgeRegressionModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "stepSize" -> 1.0, "regParam" -> 0.01, "miniBatchFraction" -> 1.0)
  
  val modelClass = (new SparkMLLibRidgeRegressionModel()).getClass
  def trainSparkModel(data: RDD[LabeledPoint]): RidgeRegressionModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val stepSize = config("stepSize").asInstanceOf[Double]
    val regParam = config("regParam").asInstanceOf[Double]
    val miniBatchFraction = config("miniBatchFraction").asInstanceOf[Double]
    return RidgeRegressionWithSGD.train(data, iterations, stepSize, regParam, miniBatchFraction)
  }
}