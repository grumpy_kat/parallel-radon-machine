package eu.ferari.spark.prm.experiments

import org.apache.spark.{SparkContext, SparkConf}
import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.log4j.Logger
import org.apache.log4j.Level

abstract class ExperimentDefinition {
  var name: String
  var clusterMode: Boolean
  
  def main(args: Array[String]): Unit = {
    var conf = new SparkConf().setAppName(name)
    if (!clusterMode) {
      System.setProperty("hadoop.home.dir", "F:/Spark/spark-1.6.0-bin-hadoop2.6")
      conf = new SparkConf().setAppName(name).setMaster("local")
    }
    
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val sparkSession = SparkSession.builder.getOrCreate()
    //val rootLogger = Logger.getRootLogger()
    //rootLogger.setLevel(Level.ERROR)
    
    run(sqlContext)
  }
  
  def run(sqlContext: SQLContext): Unit
}