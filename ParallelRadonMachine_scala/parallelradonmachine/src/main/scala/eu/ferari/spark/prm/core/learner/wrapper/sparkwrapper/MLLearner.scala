package eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper

import scala.reflect._
import eu.ferari.spark.prm.core.learner.{Learner, Model}
import eu.ferari.spark.prm.core.learner.wrapper.SparkLearner
import eu.ferari.spark.prm.prmannotations.WrapsLearner
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.ml.linalg.{Vector, SparseVector}
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel, MultilayerPerceptronClassifier, MultilayerPerceptronClassificationModel}
import org.apache.spark.ml.regression.{LinearRegression, LinearRegressionModel}
import org.apache.spark.ml.{Pipeline, PipelineModel, PredictionModel}
//import org.apache.hadoop.yarn.webapp.hamlet.HamletSpec.Block
import scala.runtime.ScalaRunTime._

/* Spark ML Learners (DataFrame-Interface)*/

abstract class SparkMLLearner[M <: SparkMLModel[SPARKMODEL] : ClassTag, SPARKMODEL <: PredictionModel[_, _]] 
  extends SparkLearner[M] {  
  
  def fit(data: Dataset[Row]): M = {
    var model : M = getNewModelInstance()
    //print(stringOf(classTag[M]))
    model.setModel(trainSparkModel(data))
    return model
  }      
  
  def trainSparkModel(data: Dataset[Row]): SPARKMODEL
}

@WrapsLearner(name = classOf[MultilayerPerceptronClassifier])
class SparkMLMultilayerPerceptronClassifier
    extends SparkMLLearner[SparkMlMultilayerPerceptronClassificationModel, MultilayerPerceptronClassificationModel] {  
  protected val config = scala.collection.mutable.Map[String, Any]("layer" -> Array[Int](3,5,4,2), "iterations" -> 1000, "blockSize" -> 128, "seed" -> 1234L, "stepSize" -> 0.03, "solver" -> "l-bfgs")
  
  def trainSparkModel(data: Dataset[Row]): MultilayerPerceptronClassificationModel = {        
    val layer = config("layer").asInstanceOf[Array[Int]]
    val blockSize = config("blockSize").asInstanceOf[Int]
    val iterations = config("iterations").asInstanceOf[Int]
    val seed = config("seed").asInstanceOf[Long]
    val stepSize = config("stepSize").asInstanceOf[Double]
    val solver = config("solver").toString()
    
    val head = data.head(1)
    val featureDim = head(0)(1).asInstanceOf[SparseVector].size
    if (layer(0) != featureDim) {
      println("\nError: number of input neurons ("+stringOf(layer(0))+") doesn't match number of features ("+stringOf(featureDim)+"). Changing number of input neurons accordingly.")
      layer(0) = featureDim
    }
    return new MultilayerPerceptronClassifier().setStepSize(stepSize).setLayers(layer).setSolver(solver).setBlockSize(blockSize).setSeed(seed).setMaxIter(iterations).fit(data)
  }
}

@WrapsLearner(name = classOf[LogisticRegression])
class SparkMLLogisticRegression
    extends SparkMLLearner[SparkMlLogisticRegressionModel, LogisticRegressionModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "regParam" -> 0.1, "elasticNetParam" -> 0.0, "fitIntercept" -> true)
  
  def trainSparkModel(data: Dataset[Row]): LogisticRegressionModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val regParam  = config("regParam").asInstanceOf[Double]
    val elasticNetParam = config("elasticNetParam").asInstanceOf[Double]
    val fitIntercept = config("fitIntercept").asInstanceOf[Boolean]
    return new LogisticRegression().setMaxIter(iterations).setRegParam(regParam).setElasticNetParam(elasticNetParam).setFitIntercept(fitIntercept).fit(data)
  }
}
 
@WrapsLearner(name = classOf[LinearRegression])
class SparkMLLinearRegression
    extends SparkMLLearner[SparkMlLinearRegressionModel, LinearRegressionModel] {
  protected val config = scala.collection.mutable.Map[String, Any]("iterations" -> 1000, "regParam" -> 0.1, "elasticNetParam" -> 0.0, "fitIntercept" -> true)

  def trainSparkModel(data: Dataset[Row]): LinearRegressionModel = {
    val iterations = config("iterations").asInstanceOf[Int]
    val regParam  = config("regParam").asInstanceOf[Double]
    val elasticNetParam = config("elasticNetParam").asInstanceOf[Double]
    val fitIntercept = config("fitIntercept").asInstanceOf[Boolean]
    return new LinearRegression().setMaxIter(iterations).setRegParam(regParam).setElasticNetParam(elasticNetParam).setFitIntercept(fitIntercept).fit(data)
  }
}