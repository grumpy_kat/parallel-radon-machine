package eu.ferari.spark.prm.core.learner.wrapper.sparkwrapper

import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.mllib.classification.{SVMModel, LogisticRegressionModel}
import org.apache.spark.mllib.regression.{LinearRegressionModel, LassoModel, RidgeRegressionModel, GeneralizedLinearModel}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.{Vector, Vectors}
import eu.ferari.spark.prm.core.learner.Model
import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/* Spark MLLib Models (RDD-Interface)*/

abstract class SparkMLLibLinearModel[M <: GeneralizedLinearModel : TypeTag] extends Model{
  var model = createModel(Vectors.dense(Array(0.0)), 0.0)
  
  protected def createModel(coefficients: Vector, intercept: Double) : M
  
  def setModel(model : M) {
    this.model = model
  }
  
  def predictPoint(features: Vector):Double = {
    return model.predict(features)
  }
  
  def predict(data: Dataset[Row]): Array[Double] = {
    import data.sqlContext.implicits._
    val rddData = data.map(row => LabeledPoint(
        row.getAs[Double]("label"), 
        Vectors.dense(row.getAs[org.apache.spark.ml.linalg.SparseVector]("features").toArray)))
    val predictions = rddData.map{ point => predictPoint(point.features) }.collect()         
    return predictions
  }
  
  def setParameters(parameters: Array[Double]) {
    this.parameters = parameters
    var weights = Vectors.dense(parameters.slice(0, parameters.length - 1))
    var intercept = parameters(parameters.length-1)
    this.model = createModel(weights, intercept)
  }
  
  def getParameters(): Array[Double] = {
    var parameters : Array[Double] = this.model.weights.toArray
    parameters = parameters :+ this.model.intercept
    return parameters
  }
}

class SparkMLLibSVMModel extends SparkMLLibLinearModel[SVMModel]{
  protected def createModel(coefficients: Vector, intercept: Double) : SVMModel = {
    val ctor = classOf[SVMModel].getDeclaredConstructor(classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    return ctor.newInstance(coefficients, new java.lang.Double(intercept)).asInstanceOf[SVMModel]
  }
}

class SparkMLLibLogisticRegressionModel extends SparkMLLibLinearModel[LogisticRegressionModel]{  
  protected def createModel(coefficients: Vector, intercept: Double) : LogisticRegressionModel = {
    val ctor = classOf[LogisticRegressionModel].getDeclaredConstructor(classOf[Vector], classOf[Double], classOf[Int], classOf[Int])
    ctor.setAccessible(true)
    return ctor.newInstance(coefficients, new java.lang.Double(intercept), new java.lang.Integer(coefficients.size), new java.lang.Integer(2)).asInstanceOf[LogisticRegressionModel]
  }
}

class SparkMLLibLinearRegressionModel extends SparkMLLibLinearModel[LinearRegressionModel]{
  protected def createModel(coefficients: Vector, intercept: Double) : LinearRegressionModel = {
    val ctor = classOf[LinearRegressionModel].getDeclaredConstructor(classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    return ctor.newInstance(coefficients, new java.lang.Double(intercept)).asInstanceOf[LinearRegressionModel]
  }
}

class SparkMLLibLassoModel extends SparkMLLibLinearModel[LassoModel]{
  protected def createModel(coefficients: Vector, intercept: Double) : LassoModel = {
    val ctor = classOf[LassoModel].getDeclaredConstructor(classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    return ctor.newInstance(coefficients, new java.lang.Double(intercept)).asInstanceOf[LassoModel]
  }
}

class SparkMLLibRidgeRegressionModel extends SparkMLLibLinearModel[RidgeRegressionModel]{
  protected def createModel(coefficients: Vector, intercept: Double) : RidgeRegressionModel = {
    val ctor = classOf[RidgeRegressionModel].getDeclaredConstructor(classOf[Vector], classOf[Double])
    ctor.setAccessible(true)
    return ctor.newInstance(coefficients, new java.lang.Double(intercept)).asInstanceOf[RidgeRegressionModel]
  }
}