package eu.ferari.spark.prm.prmannotations;
public @interface WrapsLearner {
	@SuppressWarnings("rawtypes")
	Class name();
}
