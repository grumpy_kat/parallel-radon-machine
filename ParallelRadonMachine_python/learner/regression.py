from sklearn.linear_model import Ridge
from datetime import datetime
from learner import Learner
import numpy as np
from framework.logger import log

class SKLearnLinearRegression(Learner):
    def __init__(self):
        Learner.__init__(self)
        self.identifier = "GenericSKLearnLinearRegression (please replace)"
        self.learner = None
    
    def getInitParams(self):
        return {}
        
    def train(self, X, y):
        start = datetime.now()
        self.learner.fit(X,y)
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()
    
    def predict(self, X):
        return self.learner.predict(X)
    
    def getModel(self):
        return np.array([self.learner.coef_])
    
    def setModel(self, model):
        model = model[0]
        if self.learner.coef_.shape != model.shape:
            log( "Error: new model shape does not fit current model shape. ", newline = False)
            log( "Expected: "+str(self.learner.coef_.shape)+" but got "+str(model.shape)+".")
        else:
            self.learner.coef_ = model

class RandomRegression(SKLearnLinearRegression): #for debug purposes only
    def __init__(self):
        SKLearnLinearRegression.__init__(self)
        self.identifier = "RandomRegression "
        self.learner = Ridge()
        
    def train(self, X, y):
        start = datetime.now()
        self.learner.fit(X,y)
        self.learner.coef_ = np.random.rand(self.learner.coef_.shape[0],self.learner.coef_.shape[1])
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()
    
class RidgeRegression(SKLearnLinearRegression):
    def __init__(self, alpha = 1.0): #dual = false is useful for N >> d, which we usually assume for RadonPoint-Boosting        
        self.identifier = "RidgeRegression"
        self.learner = Ridge(alpha=alpha)
        self.alpha = alpha
    
    def getInitParams(self):
        return {'alpha':self.alpha}