from learner import Learner
from datetime import datetime
import numpy as np
import sys
import math
#import statsmodels.api as st
from sklearn.linear_model import LogisticRegression
from learner.classification import SKLearnLinearClassifier
#from statsmodels.sandbox.distributions.quantize import prob_bv_rectangle
from framework.logger import log

class FeatureMap:
    def __init__(self, X, Y):
        self.length = X.shape[1] + 1
    
    def __len__(self):
        return self.length
    
    def __call__(self, x, y):
        return np.concatenate((x,np.array(y)))
    
class RealOneHot(FeatureMap):
    def __init__(self, X, Y):
        self.classes = sorted(np.unique(Y)) 
        self.length = X.shape[1] + len(self.classes)       
    
    def __call__(self, x, y):
        phi = np.copy(x)
        yOneHot = np.zeros(len(self.classes))
        for i in xrange(len(self.classes)):
            if y == self.classes[i]:
                yOneHot[i] = 1
        phi = np.concatenate((phi, yOneHot))
        return phi

class ConditionalMaxEnt(Learner):
    def __init__(self, T = 10, regParam = 0.5, eta = 0.1, featMap = RealOneHot):
        Learner.__init__(self)
        self.T = T
        self.regParam = regParam
        self.eta = eta
        self.featMap = featMap
        self.identifier = "Conditional MaxEnt ("+str(T)+")"
    
    def getInitParams(self):
        return {'T':self.T, 'regParam':self.regParam, 'eta':self.eta, 'featMap':self.featMap}
    
    def train(self, X, y):        
        start = datetime.now()
        self.classes = sorted(np.unique(y))
        self.phi = self.featMap(X,y) #feature map
        w = np.zeros(len(self.phi))
        for _ in xrange(self.T):
            grad = self.getGradient(w, self.phi, X, y)
            w = w + self.getUpdate(grad)        
        self.model = w
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()   
    
    def getUpdate(self, grad): 
        return -1*self.eta*grad #standard gradient descent with learning rate eta
    
    def getZ(self, x, w, phi):
        if self.classes == None:
            return None
        Z = 0.0
        for y in self.classes:
            prod = np.dot(w,phi(x,y))
            Z += math.exp(prod)
        return Z
    
    def getPw(self, w, phi, x, y):
        P_w = math.exp(np.dot(w, phi(x,y)))
        P_w /= self.getZ(x, w, phi)
        return P_w
    
    def getZexponents(self, x, w, phi):
        if self.classes == None:
            return None
        exps = []
        for y in self.classes:
            prod = np.dot(w,phi(x,y))
            exps.append(prod)
        return exps
    
    def getPwEfficient(self, w, phi, x, y):
        pY = math.exp(np.dot(w, phi(x,y)))
        exps = self.getZexponents(x, w, phi)
        P_w = 0.0
        for gamma in exps:
            x = math.exp(1) / (pY**(1.0 / gamma))
            P_w += x**gamma
        return 1.0/P_w
        
    def getGradient(self, w, phi, X, Y):
        m = len(X)
        grad = 2*self.regParam*np.copy(w)
        for i in xrange(m):
            x_i = X[i]
            y_i = Y[i]
            E_w = np.zeros(len(phi))
            for possY in self.classes:
                #E_w += phi(x_i, possY) * self.getPwEfficient(w, phi, x_i, possY)
                E_w += phi(x_i, possY) * self.getPw(w, phi, x_i, possY)
            grad += E_w 
            grad -= phi(x_i,y_i)
        return grad
        
#         grad = 2*self.regParam*np.copy(w)
#         m = len(X)
#         E_D = np.zeros(len(phi))
#         E_w = np.zeros(len(phi))
#         for i in xrange(m):
#             E_D += phi(X[i], y[i])
#             val = math.exp(np.dot(w, phi(X[i], y[i]))) / self.getZ(X[i], w, phi)
#             E_w += phi(X[i], y[i]) * val
#         E_D /= float(m)
#         grad += E_w - E_D
#         return grad
    
    def predict(self, X):
        y_preds = []
        for x in X:
            y_pred = self._predict(x, self.phi)
            y_preds.append(y_pred)
        return np.array(y_preds)
    
    def _predict(self, x, phi):
        y_pred = self.classes[0]
        maxLikelyhood = sys.float_info.min
        for y in self.classes:
            likelyhood = np.dot(phi(x,y),self.model)
            if likelyhood > maxLikelyhood:
                maxLikelyhood = likelyhood
                y_pred = y
        return y_pred
    
    def getModel(self):
        return np.array([self.model])
    
    def setModel(self, model):
        self.model = model[0]
        
    def __str__(self):
        return self.identifier

class SkLearnMultinomialMaxEnt(SKLearnLinearClassifier):
    def __init__(self, C=1.0, penalty='l2'):
        self.identifier = "Multinomial MaxEnt (sklearn)"
        self.learner = LogisticRegression(penalty=penalty, C=C, multi_class='multinomial', solver='newton-cg', max_iter = 1000)
        
    def getModel(self):
        model = self.vectorize(self.learner.coef_)
        return model
    
    def setModel(self, model):
        coefs = self.devectorize(model)
        if self.learner.coef_.shape != coefs.shape:
            log( "Error: new model shape does not fit current model shape. ", newline = False)
            log( "Expected: "+str(self.learner.coef_.shape)+" but got "+str(coefs.shape)+".")
        else:
            self.learner.coef_ = coefs

    def vectorize(self, coefs):
        return np.reshape(coefs, (1,-1))
        
    def devectorize(self, vec):
        return np.reshape(vec, self.learner.coef_.shape)
        
# class StatsmodelsMultinomialMaxEnt(Learner):
#     def __init__(self):
#         self.identifier = "Multinomial MaxEnt (statsmodels)"
#     
#     def train(self, X, y):
#         start = datetime.now()
#         self.learner = st.MNLogit(y, X)
#         self.resultModel = self.learner.fit()
#         self.model = self.learner.endog
#         log( self.model.shape)
#         stop = datetime.now()
#         duration = (stop - start).total_seconds()
#         return {'train':duration,'sync':0.0}, self.getModel()        
#     
#     def predict(self, X):
#         self.resultModel.predict(X)
#     
#     def setModel(self, model):
#         if self.learner.endog.shape != model.shape:
#             log( "Error: new model shape does not fit current model shape. ", newline = False)
#             log( "Expected: "+str(self.learner.coef_.shape)+" but got "+str(model.shape)+".")
#         else:
#             self.learner.endog = model