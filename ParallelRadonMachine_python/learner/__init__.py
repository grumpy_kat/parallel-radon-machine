import numpy as np
from datetime import datetime
from framework.logger import log
import itertools

class Learner:
    def __init__(self):
        self.model = np.zeros(1)
        self.isSpark = False
        self.identifier = "GenericLearner (please replace)"
    
    def getInitParams(self):
        return {}
    
    def determineSampleSize(self, X):
        return X.shape[0]
        
    def train(self, X, y):
        start = datetime.now()
        self.model = np.zeros(X.shape[1])
        stop = datetime.now()
        duration = (stop - start).total_seconds()
        return {'train':duration,'sync':0.0}, self.getModel()        
    
    def predict(self, X):
        return np.zeros(X.shape[0])
    
    def getModel(self):
        return self.model
    
    def setModel(self, model):
        self.model = model
        
    def __str__(self):
        return self.identifier
    
    def getParamRange(self):        
        initParams = self.getInitParams()
        paramRanges = {}
        for par in initParams:
            val = initParams[par]
            if not isinstance(val, bool) and isinstance(val, int) or isinstance(val, float):
                paramRanges[par] = [val * (2**x) for x in range(-5,5)]
            elif isinstance(val, bool):
                paramRanges[par] = [val] #here you could add not val, but for shuffle param this is atm not necessary
            else:
                paramRanges[par] = [val]                    
        paramNames = sorted(paramRanges.keys())        
        possParams = [dict(zip(paramNames, prod)) for prod in itertools.product(*(paramRanges[varName] for varName in paramNames))]
        print possParams
        return possParams
    
if __name__=="__main__":
    oLearner = Learner()
    X = np.zeros((5,3))
    y = np.zeros(5)    
    oLearner.train(X,y)
    y_test = oLearner.predict(X)
    log( X)
    log( y)
    log( y_test)
    log( oLearner.getModel())