import math

def sampleSizeLinear(N, steps):
    stepsize = int(math.floor(float(N-2)/float(steps-1))) if int(math.floor(float(N-2)/float(steps-1))) >= 1 else 1
    return [2+i*stepsize for i in xrange(steps)] # steps many values of n, starting with 2 and reaching (close to) N (sometimes a little less because of rounding)

