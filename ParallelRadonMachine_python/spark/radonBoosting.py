from decimal import Decimal, localcontext
import math
from datetime import datetime
from spark import SparkLearner
import numpy as np
import radonPoints.radonComputation as rc
from radonPoints.radonBoosting import MINSAMPLESIZE, getOptimalSampleSize
from framework.logger import log
from concurrent.futures import ThreadPoolExecutor, as_completed
from framework.experiment import getModelSize

class SparkRadonBoost(SparkLearner):
    def __init__(self, method, minSampleSize = MINSAMPLESIZE, h=-1):
        self.identifier = "SparkRadonBoost("+str(method)+")"
        self.minSampleSize = minSampleSize
        self.h = h
        self.learner = method
        self.model = None
    
    def determineSampleSize(self, X):
        instCount = X.shape[0]
        R = rc.getRadonNumber(X)
        self.minSampleSize = getOptimalSampleSize(instCount, R)            
            
    def train(self, data, bRegression = False):        
        self.bRegression = bRegression
        instCount = data.count()
        R = getModelSize(data, self.learner, instCount) + 2
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1
            log( "Using height "+str(self.h)+" not possible, too few data points for min-sample-size "+str(self.minSampleSize)+". Using height "+str(h)+" instead.")
        
        baseSampleSize = int(instCount/(R**h))
        c = R**h
        
        modelVecs = []
        trainduration = 0.0        
        if self.learner.isSpark:            
            weights = [1.0]*c
            dataChunks = data.randomSplit(weights)
            executor = ThreadPoolExecutor(max_workers = c)
            futures = []
            start = datetime.now()
            with ThreadPoolExecutor(max_workers = c) as executor:
                for chunk in dataChunks:
                    futures.append(executor.submit(self.learner.train, chunk))             
            for f in as_completed(futures):
                modelVecs.append(self.obtainModelFromFuture(f))
            stop = datetime.now()
            trainduration += (stop - start).total_seconds()
        else:
            partData = self.getStratifiedPartitioning(data, R**h, baseSampleSize)       
            start = datetime.now()                  
            modelVecs = partData.mapPartitions(applyLearnerToPartition(self.learner)).collect()        
            stop = datetime.now()
            trainduration += (stop - start).total_seconds()
        trainduration /= float(R**h)
        
             
        if len(modelVecs) != c:
            log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(modelVecs))+" models have been created. ", newline = False)
            log( "(N:"+str(instCount)+" D:"+str(R-2)+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")
        S = np.array(modelVecs)        
        start = datetime.now() 
        optimalModel = np.array([rc.getRadonPointHierarchical(S, h)]) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
        self.learner.setModel(optimalModel)
        stop = datetime.now()        
        overhead = (math.log(float(c))/math.log(float(R)))
        syncduration = ((stop - start).total_seconds() / float(c))*overhead
        return {'train':trainduration,'sync':syncduration}, self.learner.getModel()
        
    def predict(self, testData):
        if self.learner.isSpark:
            return self.learner.predict(testData).collect()
        else:
            X = np.array(testData.collect())
            return self.learner.predict(X)
    
    def obtainModelFromFuture(self, future):
        _, model = future.result()
        return model[0]
    
    def sparkModelToArray(self, model):
        return np.append(model.weights.toArray(), model.intercept)
    
    def getStratifiedSample(self, indices, y, noOfSamples, baseSampleSize):
        if self.bRegression:
            np.random.shuffle(indices)
            samples = [s for s in np.array_split(indices, noOfSamples)]
            return samples
        if len(np.unique(y)) == 2:
            return self.getStratifiedSampleBinary(indices, y, noOfSamples, baseSampleSize)
        else:
            return self.getStratifiedSampleMulticlass(indices, y, noOfSamples, baseSampleSize)

    def getStratifiedSampleMulticlass(self, indices, y, noOfSamples, baseSampleSize):
        classes = np.unique(y)
        samples = {}
        for c in classes:
            idx = [i for i in indices if y[i] == c]       
            sampleSize = int(float(len(idx)) / float(len(indices)) * baseSampleSize)      
            c_samples = np.split(idx, [i*sampleSize for i in xrange(1,noOfSamples)])
            samples[c] = c_samples
        sampleList = []
        for i in xrange(noOfSamples):
            s = np.array([]).astype(int)
            for c in classes:
                s = np.append(s, samples[c][i])
            sampleList.append(s.astype(int))
        return sampleList
  
    def getStratifiedSampleBinary(self, indices, y, noOfSamples, baseSampleSize):
        pos = [i for i in indices if y[i] == 1.0]
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        posSampleSize = int(float(len(pos)) / float(len(indices)) * baseSampleSize)
        negSampleSize = int(float(len(neg)) / float(len(indices)) * baseSampleSize)      
        pos_samples = np.split(pos, [i*posSampleSize for i in xrange(1,noOfSamples)])
        neg_samples = np.split(neg, [i*negSampleSize for i in xrange(1,noOfSamples)])
        if len(pos_samples) != len(neg_samples):
            log( "Error: wanted the same number of samples but got something different for positive and negative examples...")
        samples = []        
        for i in xrange(len(pos_samples)):
            samples.append(np.append(pos_samples[i], neg_samples[i]).astype(int))
        return samples
    
    def getStratifiedPartitioning(self, data, numPartitions, baseSampleSize = 2):        
        y = np.array(data.map(lambda lp: lp.label).collect())
        N = len(y)
        noOfSamples = N / numPartitions
        sample_indices = self.getStratifiedSample(range(N), y, noOfSamples, baseSampleSize)
        keyMap = {}
        #log("Sample Idxs: ", sample_indices)
        for key in xrange(N):
            part = -1
            for i in xrange(len(sample_indices)):
                if key in sample_indices[i]:
                    part = i
            if part == -1:
                log("Error in stratified sampling!!!")
            keyMap[key] = part
        partitionedData = data.zipWithIndex().map(lambda (value, key): (keyMap[key] ,value)).repartitionAndSortWithinPartitions(numPartitions=numPartitions).values()
        return partitionedData
        

#This class is only there to speed up experiments. It works for the currently used learner classes but makes a few assumptions 
#(e.g., all learners have the same Radon number, all use the same h, etc.) which can be wrong in general. Thus, use it with care!
#Moreover, it does not work with Spark-Learners!
class SparkRadonBoostOnLearnerSet(SparkRadonBoost):
    def __init__(self, methods, minSampleSize = MINSAMPLESIZE, h=-1):
        self.identifier = "SparkRadonBoostLearnerSet("+str(methods)+")"
        self.minSampleSize = minSampleSize
        self.h = h
        self.learners = []
        for method in methods:
            if not method.isSpark:
                self.learners.append(method)        
        self.model = None
    
    def getExpIdentifier(self, learner):
        return "SparkRadonBoostls("+str(learner)+")"
        
    def train(self, data, bRegression = False):        
        self.bRegression = bRegression
        instCount = data.count()
        R = 2 #I assume all learners have the same model size here, this could turn out to be wrong in the future...
        for learner in self.learners:
            R = getModelSize(data, learner, instCount) + 2
        h = self.h
        if h < 1:
            h = int(math.log(float(instCount) / float(self.minSampleSize),float(R)))        
        if instCount/(R**self.h) <= self.minSampleSize:            
            while instCount/(R**h) <= self.minSampleSize:
                h -= 1
            log( "Using height "+str(self.h)+" not possible, too few data points for min-sample-size "+str(self.minSampleSize)+". Using height "+str(h)+" instead.")
        
        baseSampleSize = int(instCount/(R**h))
        c = R**h
        
        modelVecs = []
        trainduration = 0.0        
        partData = self.getStratifiedPartitioning(data, R**h, baseSampleSize)
        durations = {}       
        models = {}
        for learner in self.learners:
            start = datetime.now()                  
            modelVecs = partData.mapPartitions(applyLearnerToPartition(learner)).collect()        
            stop = datetime.now()
            trainduration += (stop - start).total_seconds()
            trainduration /= float(R**h)
                     
            if len(modelVecs) != c:
                log( "Something went wrong. Expected number of model: "+str(R**h)+" but "+str(len(modelVecs))+" models have been created. ", newline = False)
                log( "(N:"+str(instCount)+" D:"+str(R-2)+" h:"+str(h)+" sampleSize:"+str(baseSampleSize)+")")
            S = np.array(modelVecs)        
            start = datetime.now() 
            optimalModel = np.array([rc.getRadonPointHierarchical(S, h)]) #works like this only for binary classification and regression, where there is only one linear model per learner. For multiclas, something else must be done here.
            learner.setModel(optimalModel)
            stop = datetime.now()        
            overhead = (math.log(float(c))/math.log(float(R)))
            syncduration = ((stop - start).total_seconds() / float(c))*overhead
            durations[learner] = {'train':trainduration,'sync':syncduration}
            models[learner] = learner.getModel()
        return durations, models
        
    def predict(self, testData):
        predictions = {}
        for learner in self.learners:
            X = np.array(testData.collect())
            predictions[learner] = learner.predict(X)
        return predictions
            
def applyLearnerToPartition(learner):
    def _applyLearnerToPartition(iterator):
        X = []
        Y = []
        for p in iterator:
            x = p.features
            y = p.label
            X.append(x.toArray())
            Y.append(y)
        learner.train(X,Y)
        return learner.getModel()
    return _applyLearnerToPartition
    
    