import os
import arff
import collections as cl
import numpy as np
import spark
from framework.logger import log
from data.synthetic import RapidlyDriftingDisjunction, BshoutyLongModel
from pyspark.mllib.regression import LabeledPoint
from pyspark.mllib.feature import StandardScaler
from pyspark.mllib.linalg import Vectors
from pyspark.sql import SQLContext
from lib2to3.fixer_util import Newline
from sklearn import preprocessing

SUPPORTED_FILE_TYPES = [".data", ".arff", ".csv", ".txt"]

class Datasets:
    def __init__(self, addConstant = None, normalize = False, sparkNormalization = True):
        self.datapath = os.path.dirname(spark.rootPath+"/data/")+"/"
        self.datasets = cl.defaultdict(list)    
        self.datasetInfo = {}
        self.sc = spark.globalSparkContext
        log.sc = self.sc
        log.mode = log.SPARK  # @UndefinedVariable
        sqlContext = SQLContext(self.sc)        # @UnusedVariable
        for stFile in os.listdir(self.datapath+"classification"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["classification"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"largeclassification"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["largeclassification"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"multi-class"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["multi-class"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"largemulti-class"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["largemulti-class"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"regression"):
            if stFile.endswith(".data"):
                self.datasets["regression"].append(stFile.replace(".data",""))
        for stFile in os.listdir(self.datapath+"largeregression"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["largeregression"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"hugeimbalanced"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["hugeimbalanced"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break            
        for stFile in os.listdir(self.datapath+"highDimlowH"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["highDimlowH"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break        
        for stFile in os.listdir(self.datapath+"sparkDatasets"):
            if stFile.endswith(".dat"):
                self.datasets["sparkDatasets"].append(stFile.replace(".dat",""))                           
        self.constant = addConstant
        self.bNormalize = normalize
        self.sparkNormalization = sparkNormalization
        #self.datasets["classification"].append("synthetic_disjunctions")
    
    def getAllDatasetNames(self):
        data = []
        for l in self.datasets.values():
            data += l
        return data
    
    def getTaskNames(self):
        return self.datasets.keys()
    
    def getDatasetTask(self, name):
        if "synthetic" in name:
                name = name[:name.find("(")]
                return name
        for task in self.datasets:            
            if name in self.datasets[task]:
                return task
        return "unknown"
    
    def getDatasetNames(self, task):
        return self.datasets[task]
    
    def getDataset(self, name, missingVal = 0.0):
        log("Now loading dataset " + name + " ...")
#         if "synthetic" in name:
#             X,y = self.createSynthetic(name)       
        dataset = ""
        N = None
        labels = None        
        for root, _, files in os.walk(self.datapath):
            if '.svn' in root:
                continue
            for filename in files:
                if name == os.path.splitext(filename)[0]:
                    dataset = os.path.join(root, filename)
        if ".arff" in dataset:
            data, N, labels = self.loadArff(name, dataset, missingVal)
        if ".data" in dataset:
            #data = self.sc.loadLibSVMFile(dataset)
            data, N, labels = self.loadSvmLightStyle(name, dataset, missingVal)
        if ".csv" in dataset:
            data, N, labels = self.loadCSV(name, dataset, missingVal)
        if ".txt" in dataset:
            log( "Textfile interpreted as libSVM style input."  )          
            #data = self.sc.loadLibSVMFile(dataset)
            data, N, labels = self.loadSvmLightStyle(name, dataset, missingVal)
        if ".dat" in dataset:
            data = self.loadSparkDataset(name, dataset)
        #add dataset info
        #TODO: implement sampling for spark data
        if "sampled" in name:
            datasetName = name[name.find('(')+1:name.find(',')]
            size = int(name[name.find(',')+1:name.find(')')])
            data = self.getDataset(datasetName, missingVal)
            data = self.sample(data, size)
            self.datasetInfo[name] = self.datasetInfo[datasetName]
            for datatype in self.datasets:
                if datasetName in self.datasets[datatype]:
                    self.datasets[datatype].append(name)        
        if self.constant != None:
            log(" adding constant (for offset)... ")
            data = self.addConstant(data, self.constant)        
        if self.bNormalize and self.sparkNormalization:
            log(" normalizing data... ")
            data = self.normalize(data)
        if name in self.datasets["classification"] or name in self.datasets["largeclassification"] or name in self.datasets["multi-class"] or name in self.datasets["hugeimbalanced"] or name in self.datasets["highDimlowH"]:
            pass
            if N == None:
                N = data.count()
            labelCounts = []    
            numClasses = 0
            if labels == None:        
                labels = data.map(lambda p: p.label)
                labelCounts = labels.countByValue()
                numClasses = len(labelCounts.keys())
            else:
                uniqueLabels, counts = np.unique(labels, return_counts = True)
                labelCounts = dict(zip(uniqueLabels, counts))
                numClasses = len(uniqueLabels)
             
            classBalance = [0.0] * numClasses
            for i in xrange(numClasses):
                classBalance[i] = float(labelCounts[labelCounts.keys()[i]]) / float(N) 
            self.datasetInfo[name]['numClasses'] = numClasses
            self.datasetInfo[name]['classCounts'] = labelCounts
            self.datasetInfo[name]['classBalance'] = classBalance        
            log("N: "+str(N)+" class balance: ", classBalance, "...")    
        elif name in self.datasets["regression"] or name in self.datasets["largeregression"]:
            labels = data.map(lambda p: p.label)
            mean = labels.mean()
            std = labels.stdev()
            self.datasetInfo[name]['mean'] = mean
            self.datasetInfo[name]['std'] = std
        log("done.", newline=True)
        return data
    
    def loadSparkDataset(self, name, dataset):
        data = None
        if "local[*]" not in self.sc.master:
            try:
                data = self.sc.textFile("hdfs:///user/mkamp/sparkDatasets/"+name+".dat")
                data.take(1)
                log("Loaded dataset from hdfs.")
            except:
                data = None
                log( "Could not load dataset",dataset," from hdfs. Using local file instead.")
        if data == None:
            dataset = dataset.replace("\\","/")
            data = self.sc.textFile(dataset)
            log("Loaded local dataset.")
        data = data.map(lambda line: [float(x) for x in line.split(",")]).map(lambda x: LabeledPoint(x[0], x[1:]))
        instCount = data.count()
        dim = len(data.take(1)[0].features)
        self.datasetInfo[name] = {"N":instCount, "D":dim, "task":"classification"}
        return data
    
    def loadSvmLightStyle(self, name, filename, missingVal):        
        f = open(filename, "r")        
        features = []
        labels = []
        classes = []
        for line in f.readlines():
            vals = line.replace("\n","").split(" ")
            if vals[0] not in classes:
                classes.append(vals[0])            
            labels.append(vals[0])
            features.append(cl.defaultdict(float))            
            for val in vals[1:]:
                if ":" in val:
                    feat = val.split(":")
                    val = feat[1]
                    if val == None:
                        val = missingVal                
                    features[-1][int(feat[0])] = float(val)
        y_list = []
        for l in labels:
            y_list.append(float(classes.index(l)))
        fidx_min = 10000000000
        fidx_max = 0
        for f in features:
            feature_indizes = sorted(f.keys(), key=float)
            if float(feature_indizes[0]) < fidx_min:
                fidx_min = int(feature_indizes[0])
            if float(feature_indizes[-1]) > fidx_max:
                fidx_max = int(feature_indizes[-1])
        featCount = fidx_max - fidx_min
        instCount = len(features)
        X = np.zeros((instCount,featCount))
        for i in xrange(len(features)):
            f = features[i]            
            for j in xrange(fidx_min,fidx_max):
                featureValue = f[j]
                X[i][j-int(fidx_min)] = featureValue         
        self.datasetInfo[name] = {"N":instCount, "D":featCount, "task":self.getDatasetTask(name)}    
        if not self.sparkNormalization and self.bNormalize:
            self.normalizeSklearn(X)
            numerical_data = X.tolist()
        rawData = []
        for i in xrange(len(y_list)):
            label = y_list[i]
            features = Vectors.dense(numerical_data[i])
            rawData.append(LabeledPoint(label, features))  
        
        data = self.sc.parallelize(rawData)
        return data, instCount, np.array(y_list)
    
    def loadArff(self, name, filename, missingVal, binarizeCategoricalData = True):
        data = arff.load(open(filename))
        #attribute_names = np.array([str(x[0]) for x in data['attributes']])
        considered = []
        categorical_data = []
        categories = {}
        numerical_data = []
        labelCol = -1
        labels = []
        for i,t in enumerate(data['attributes']):
            if 'class' in str(t[0]).lower():
                labelCol = i
            else:
                att = str(t[1]).lower()
                if att in ['real', 'integer', 'numeric','[u\'-1\', u\'1\', u\'0\']', '[u\'-1\', u\'1\']']:
                    considered.append(i)
                elif isinstance(t[1], list):
                    categorical_data.append(i)
                    categories[i] = t[1]
        if labelCol < 0:
            log( "Error: no label found. " + name)
        for i, line in enumerate(data['data']):
            row = [float(line[j]) if line[j] is not None else missingVal for j in considered]
            if binarizeCategoricalData:
                for col in categorical_data:
                    indexArray = [0.0]*len(categories[col])
                    val = categories[col].index(line[col])
                    indexArray[val] = 1.0
                    row += indexArray
            numerical_data.append(row)
            labels.append(line[labelCol])
        classes = []
        for label in labels:
            if label not in classes:
                classes.append(label)    
        y_list = []
        for l in labels:
            y_list.append(float(classes.index(l)))            
        self.datasetInfo[name] = {"N":len(numerical_data), "D":len(numerical_data[0]), "task":self.getDatasetTask(name)}
        if not self.sparkNormalization and self.bNormalize:
            X = np.array(numerical_data)
            self.normalizeSklearn(X)
            numerical_data = X.tolist()
        rawData = []
        for i in xrange(len(y_list)):
            label = y_list[i]
            features = Vectors.dense(numerical_data[i])
            rawData.append(LabeledPoint(label, features))  
        
        data = self.sc.parallelize(rawData)
        return data, len(numerical_data), np.array(y_list)
    
    def loadCSV(self, name, filename, missingVal):
        f = open(filename, "r")        
        features = []
        labels = []
        classes = []
        count = 0
        #maxInstCount = 800000
        for line in f:
            count += 1
            #if count > maxInstCount:
            #    break
            vals = line.replace("\n","").split(",")
            if float(vals[0]) not in classes:
                classes.append(float(vals[0]))            
            labels.append(float(vals[0]))
            features.append([])            
            for val in vals[1:]:
                if val == None:
                    val = missingVal                
                features[-1].append(float(val))
        y_list = []
        for l in labels:
            y_list.append(float(classes.index(l)))
        featCount = len(features[0])
        instCount = len(features)
        X = np.zeros((instCount,featCount))
        for i in xrange(len(features)):
            f = features[i]            
            for j in xrange(len(f)):
                featureValue = f[j]
                X[i][j] = featureValue         
        self.datasetInfo[name] = {"N":instCount, "D":featCount, "task":self.getDatasetTask(name)}     
        if not self.sparkNormalization:
            self.normalizeSklearn(X)
        rawData = []
        for i in xrange(len(y_list)):
            rawData.append(LabeledPoint(y_list[i], X[i]))        
        data = self.sc.parallelize(rawData)
        y= np.array(y_list).reshape((len(y_list),1))
        Z = np.concatenate((y,X), axis = 1)
        np.savetxt(name + ".dat", Z, delimiter = ",")
        N = data.count()
        print N, " successfully executed parallelize."        
        return data, instCount, np.array(y_list)
    
    def createSynthetic(self, name): #convention is to have name as "<synthetic_type>(<instCount>,<featCount>)"
        className = ""
        if "synthetic_disjunctions" in name:
            className = RapidlyDriftingDisjunction
        if "synthetic_bshouty" in name:
            className = BshoutyLongModel
        if "(" not in name:
            log( "Error: " + name)
            return
        args = name[name.find("(")+1:name.find(")")]
        args = args.split(",")
        instCount = int(args[0])
        dim = int(args[1])
        input_stream = className(dim, 0.0)
        X = []
        y = []
        for _ in xrange(instCount):
            (features, label) = input_stream.generate_example()
            if label == -1:
                label = 0
            X.append(features.toList(dim))
            y.append(label)
        self.datasetInfo[name] = {"N":instCount, "D":dim, "task":self.getDatasetTask(name)}
        rawData = []
        for i in xrange(len(y)):
            rawData.append(LabeledPoint(y[i], X[i]))        
        data = self.sc.parallelize(rawData)        
        return data, instCount, np.array(y)
    
    def getDatasetInfo(self, dataset):
        if dataset not in self.datasetInfo:
            self.getDataset(dataset)
        return self.datasetInfo[dataset]

    def addConstant(self, data, constant = 1.0):    
        data = data.map(lambda p: LabeledPoint(p.label, np.append(p.features.toArray(), constant)))
        return data
    
    def normalize(self, data):
        label = data.map(lambda x: x.label)
        features = data.map(lambda x: x.features)
        scaler = StandardScaler(withMean=True, withStd=True).fit(features)
        return label.zip(scaler.transform(features.map(lambda x: Vectors.dense(x.toArray())))).map(lambda x: LabeledPoint(x[0],x[1]))
    
    def normalizeSklearn(self, X):        
        X_new = preprocessing.scale(X)     
        return X_new
    
    def sample(self, data, size):
        fraction = float(size) / float(len(data))
        return data.sample(withReplacement = False, fraction = fraction)
        
    def getSingleStratifiedSample(self, indices, y, sampleSize, bRegression = False):
        if bRegression:
            np.random.shuffle(indices)
            return indices[:sampleSize]
        if len(np.unique(y)) == 2:
            return self.getSingleStratifiedSampleBinary(indices, y, sampleSize)
        else:
            return self.getSingleStratifiedSampleMulticlass(indices, y, sampleSize)

    def getSingleStratifiedSampleMulticlass(self, indices, y, sampleSize):
        classes = np.unique(y)
        samples = {}
        overhead = 0
        for c in classes:
            idx = [i for i in indices if y[i] == c]       
            np.random.shuffle(indices)
            size = int(float(len(idx)) / float(len(indices)) * sampleSize)
            if overhead > 0 and size > 1:
                size -= 1                
            if size == 0:
                size = 1
                overhead += 1
            c_sample = indices[:size]
            samples[c] = c_sample             
        s = np.array([]).astype(int)
        for c in classes:
            s = np.append(s, samples[c])
        count = 0
        while len(s) < sampleSize and count < sampleSize * 10:
            np.random.shuffle(indices)
            idx = indices[0]
            if idx not in s:
                s = np.append(s, [idx])
            count += 1
        s.astype(int)
        return s
  
    def getSingleStratifiedSampleBinary(self, indices, y, sampleSize):
        np.random.shuffle(indices)  
        pos = [i for i in indices if y[i] == 1.0]
        np.random.shuffle(indices)  
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        posSampleSize = int(float(len(pos)) / float(len(indices)) * sampleSize)
        negSampleSize = int(float(len(neg)) / float(len(indices)) * sampleSize)    
        if posSampleSize == 0:
            posSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if negSampleSize > 1:
                    negSampleSize -=1
        if negSampleSize == 0:
            negSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if posSampleSize > 1:
                    posSampleSize -=1
        pos_samples = pos[:posSampleSize]
        neg_samples = neg[:negSampleSize]
        s = np.append(pos_samples,neg_samples)
        count = 0
        while len(s) < sampleSize and count < sampleSize * 10:
            np.random.shuffle(indices)
            idx = indices[0]
            if idx not in s:
                s = np.append(s, [idx])
            count += 1
        s.astype(int)
        return s    
    
class SparkDatasets(Datasets):
    def __init__(self, addConstant = None, normalize = False, sparkNormalization = True):
        self.datapath = os.path.dirname(spark.rootPath+"/data/")+"/"
        self.datasets = cl.defaultdict(list)    
        self.datasetInfo = {}
        self.sc = spark.globalSparkContext
        log.sc = self.sc
        log.mode = log.SPARK  # @UndefinedVariable
        sqlContext = SQLContext(self.sc)        # @UnusedVariable   
        for stFile in os.listdir(self.datapath+"sparkDatasets"):
            if stFile.endswith(".dat"):
                self.datasets["sparkDatasets"].append(stFile.replace(".dat",""))                           
        self.constant = addConstant
        self.bNormalize = normalize
        self.sparkNormalization = sparkNormalization
        
    def getDataset(self, name, missingVal = 0.0):
        log("Now loading dataset " + name + " ...")
#         if "synthetic" in name:
#             X,y = self.createSynthetic(name)       
        dataset = ""   
        for root, _, files in os.walk(self.datapath):
            if '.svn' in root:
                continue
            for filename in files:
                if name == os.path.splitext(filename)[0] and filename.endswith(".dat"):                    
                    dataset = os.path.join(root, filename)
                    
        data = self.loadSparkDataset(name, dataset)     
             
        if self.constant != None:
            log(" adding constant (for offset)... ")
            data = self.addConstant(data, self.constant)        
        if self.bNormalize:
            log(" normalizing data... ")
            data = self.normalize(data)        
        N = self.datasetInfo[name]["N"]
        labels = data.map(lambda p: p.label)
        labelCounts = labels.countByValue()
        numClasses = len(labelCounts.keys())
         
        classBalance = [0.0] * numClasses
        for i in xrange(numClasses):
            classBalance[i] = float(labelCounts[labelCounts.keys()[i]]) / float(N) 
        self.datasetInfo[name]['numClasses'] = numClasses
        self.datasetInfo[name]['classCounts'] = labelCounts
        self.datasetInfo[name]['classBalance'] = classBalance        
        log("N: "+str(N)+" class balance: ", classBalance, "...")    
        log("done.", newline=True)
        return data
    
    def loadSparkDataset(self, name, dataset):
        data = None
        if "local[*]" not in self.sc.master:
            try:
                data = self.sc.textFile("hdfs:///user/mkamp/sparkDatasets/"+name+".dat")
                data.take(1)
                log("Loaded dataset from hdfs.")
            except:
                data = None
                log( "Could not load dataset",dataset," from hdfs. Using local file instead.")
        if data == None:
            #dataset = dataset.replace("\\","/")
            dataset = dataset.replace("/","\\")
            data = self.sc.textFile(dataset).cache()
            print data.take(1)
            log("Loaded local dataset.")
        data = data.map(lambda line: [float(x) for x in line.split(",")]).map(lambda x: LabeledPoint(x[0], x[1:]))
        instCount = data.count()
        dim = len(data.take(1)[0].features)
        self.datasetInfo[name] = {"N":instCount, "D":dim, "task":"classification"}
        return data
    
if __name__ == "__main__":
    data = Datasets()
    log( data.getAllDatasetNames())
    log( data.getTaskNames())
    log( data.getDatasetNames("classification"))
    X,y = data.getDataset("colic")
    log( X)
    log( y)