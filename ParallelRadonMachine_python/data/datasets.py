import os

import arff
from framework.logger import log
import collections as cl
import numpy as np
#import sparse_vector
from synthetic import RapidlyDriftingDisjunction, BshoutyLongModel
from sklearn import preprocessing


SUPPORTED_FILE_TYPES = [".data", ".arff", ".csv"]

class Datasets:
    def __init__(self, addConstant = None, normalize = False):
        self.datapath = os.path.dirname(os.path.abspath(__file__))+"/"
        self.datasets = cl.defaultdict(list)    
        self.datasetInfo = {}
        for stFile in os.listdir(self.datapath+"classification"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["classification"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"largeclassification"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["largeclassification"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"multi-class"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["multi-class"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"largemulti-class"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["largemulti-class"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"regression"):
            if stFile.endswith(".data"):
                self.datasets["regression"].append(stFile.replace(".data",""))
        for stFile in os.listdir(self.datapath+"largeregression"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["largeregression"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break
        for stFile in os.listdir(self.datapath+"hugeimbalanced"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["hugeimbalanced"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break            
        for stFile in os.listdir(self.datapath+"highDimlowH"):
            for i in xrange(len(SUPPORTED_FILE_TYPES)):
                if stFile.endswith(SUPPORTED_FILE_TYPES[i]):
                    self.datasets["highDimlowH"].append(stFile.replace(SUPPORTED_FILE_TYPES[i],""))
                    break                  
        self.constant = addConstant
        self.bNormalize = normalize
        #self.datasets["classification"].append("synthetic_disjunctions")
    
    def getAllDatasetNames(self):
        data = []
        for l in self.datasets.values():
            data += l
        return data
    
    def getTaskNames(self):
        return self.datasets.keys()
    
    def getDatasetTask(self, name):
        if "synthetic" in name:
                name = name[:name.find("(")]
                return name
        for task in self.datasets:            
            if name in self.datasets[task]:
                return task
        return "unknown"
    
    def getDatasetNames(self, task):
        return self.datasets[task]
    
    def getDataset(self, name, missingVal = 0.0):
        if "synthetic" in name:
            X,y = self.createSynthetic(name)       
        dataset = ""
        for root, _, files in os.walk(self.datapath):
            if '.svn' in root:
                continue
            for filename in files:
                if name == os.path.splitext(filename)[0]:
                    dataset = os.path.join(root, filename)
        if ".arff" in dataset:
            X,y = self.loadArff(name, dataset, missingVal)
        if ".data" in dataset:
            X,y = self.loadSvmLightStyle(name, dataset, missingVal)
        if ".csv" in dataset:
            X,y = self.loadCSV(name, dataset, missingVal)        
        #add dataset info
        if "sampled" in name:
            datasetName = name[name.find('(')+1:name.find(',')]
            size = int(name[name.find(',')+1:name.find(')')])
            Xorig,yorig = self.getDataset(datasetName, missingVal)
            X,y = self.sample(Xorig, yorig, size)
            self.datasetInfo[name] = self.datasetInfo[datasetName]
            for datatype in self.datasets:
                if datasetName in self.datasets[datatype]:
                    self.datasets[datatype].append(name)
        if self.constant != None:
            X = self.addConstant(X, self.constant)
        if self.bNormalize:
            X = self.normalize(X)
        if name in self.datasets["classification"] or name in self.datasets["largeclassification"] or name in self.datasets["multi-class"] or name in self.datasets["hugeimbalanced"] or name in self.datasets["highDimlowH"]:
            classes = {}
            for l in y:
                if l not in classes:
                    classes[l] = 1
                else:
                    classes[l] += 1
            classBalance = [0.0] * len(classes.keys())
            for i in xrange(len(classes.keys())):
                classBalance[i] = float(classes[classes.keys()[i]]) / float(len(y)) 
            self.datasetInfo[name]['numClasses'] = len(classes.keys())
            self.datasetInfo[name]['classCounts'] = classes
            self.datasetInfo[name]['classBalance'] = classBalance            
        elif name in self.datasets["regression"] or name in self.datasets["largeregression"]:
            mean = np.mean(y)
            std = np.std(y)
            self.datasetInfo[name]['mean'] = mean
            self.datasetInfo[name]['std'] = std
        return X,y
        
    
    def loadArff(self, name, filename, missingVal, binarizeCategoricalData = True):
        data = arff.load(open(filename))
        #attribute_names = np.array([str(x[0]) for x in data['attributes']])
        considered = []
        categorical_data = []
        categories = {}
        numerical_data = []
        labelCol = -1
        labels = []
        for i,t in enumerate(data['attributes']):
            if 'class' in str(t[0]).lower():
                labelCol = i
            else:
                att = str(t[1]).lower()
                if att in ['real', 'integer', 'numeric','[u\'-1\', u\'1\', u\'0\']', '[u\'-1\', u\'1\']']:
                    considered.append(i)
                elif isinstance(t[1], list):
                    categorical_data.append(i)
                    categories[i] = t[1]
        if labelCol < 0:
            log( "Error: no label found. " + name)
        for i, line in enumerate(data['data']):
            row = [float(line[j]) if line[j] is not None else missingVal for j in considered]
            if binarizeCategoricalData:
                for col in categorical_data:
                    indexArray = [0.0]*len(categories[col])
                    val = categories[col].index(line[col])
                    indexArray[val] = 1.0
                    row += indexArray
            numerical_data.append(row)
            labels.append(line[labelCol])
        classes = []
        for label in labels:
            if label not in classes:
                classes.append(label)    
        y_list = []
        for l in labels:
            y_list.append(float(classes.index(l)))
        self.datasetInfo[name] = {"N":len(numerical_data), "D":len(numerical_data[0]), "task":self.getDatasetTask(name)}
        return np.array(numerical_data), np.array(y_list)
    
    def loadCSV(self, name, filename, missingVal):
        f = open(filename, "r")        
        features = []
        labels = []
        classes = []
        count = 0
        #maxInstCount = 800000
        for line in f:
            count += 1
            #if count > maxInstCount:
            #    break
            vals = line.replace("\n","").split(",")
            if float(vals[0]) not in classes:
                classes.append(float(vals[0]))            
            labels.append(float(vals[0]))
            features.append([])            
            for val in vals[1:]:
                if val == None:
                    val = missingVal                
                features[-1].append(float(val))
        y_list = []
        for l in labels:
            y_list.append(float(classes.index(l)))
        featCount = len(features[0])
        instCount = len(features)
        X = np.zeros((instCount,featCount))
        for i in xrange(len(features)):
            f = features[i]            
            for j in xrange(len(f)):
                featureValue = f[j]
                X[i][j] = featureValue
        y = np.array(y_list)           
        self.datasetInfo[name] = {"N":instCount, "D":featCount, "task":self.getDatasetTask(name)}     
        return X,y
    
    def loadSvmLightStyle(self, name, filename, missingVal):        
        f = open(filename, "r")        
        features = []
        labels = []
        classes = []
        for line in f.readlines():
            vals = line.replace("\n","").split(" ")
            if vals[0] not in classes:
                classes.append(vals[0])            
            labels.append(vals[0])
            features.append(cl.defaultdict(float))            
            for val in vals[1:]:
                if ":" in val:
                    feat = val.split(":")
                    val = feat[1]
                    if val == None:
                        val = missingVal                
                    features[-1][int(feat[0])] = float(val)
        y_list = []
        for l in labels:
            y_list.append(float(classes.index(l)))
        fidx_min = 10000000000
        fidx_max = 0
        for f in features:
            feature_indizes = sorted(f.keys(), key=float)
            if float(feature_indizes[0]) < fidx_min:
                fidx_min = int(feature_indizes[0])
            if float(feature_indizes[-1]) > fidx_max:
                fidx_max = int(feature_indizes[-1])
        featCount = fidx_max - fidx_min
        instCount = len(features)
        X = np.zeros((instCount,featCount))
        for i in xrange(len(features)):
            f = features[i]            
            for j in xrange(fidx_min,fidx_max):
                featureValue = f[j]
                X[i][j-int(fidx_min)] = featureValue
        y = np.array(y_list)           
        self.datasetInfo[name] = {"N":instCount, "D":featCount, "task":self.getDatasetTask(name)}    
        return X,y
    
    def createSynthetic(self, name): #convention is to have name as "<synthetic_type>(<instCount>,<featCount>)"
        className = ""
        if "synthetic_disjunctions" in name:
            className = RapidlyDriftingDisjunction
        if "synthetic_bshouty" in name:
            className = BshoutyLongModel
        if "(" not in name:
            log( "Error: " + name)
            return
        args = name[name.find("(")+1:name.find(")")]
        args = args.split(",")
        instCount = int(args[0])
        dim = int(args[1])
        input_stream = className(dim, 0.0)
        X = []
        y = []
        for _ in xrange(instCount):
            (features, label) = input_stream.generate_example()
            if label == -1:
                label = 0
            X.append(features.toList(dim))
            y.append(label)
        self.datasetInfo[name] = {"N":instCount, "D":dim, "task":self.getDatasetTask(name)}
        return np.array(X),np.array(y)
    
    def getDatasetInfo(self, dataset):
        if dataset not in self.datasetInfo:
            self.getDataset(dataset)
        return self.datasetInfo[dataset]

    def addConstant(self, X, constant = 1.0):    
        consts = np.ones((X.shape[0],1))*constant
        X = np.hstack((X, consts))
        return X
    
    def normalize(self, X):        
        X_new = preprocessing.scale(X)     
        return X_new
    
    def sample(self, Xorig, yorig, size):
        xlist = ()
        ylist = ()
        for _ in xrange(int(size/len(Xorig))):
            xlist += (Xorig,)
            ylist += (yorig,)      
        rest = size % len(Xorig) 
        if rest > 0: 
            indices = range(len(Xorig))
            np.random.shuffle(indices)
            sampleIdxs = self.getSingleStratifiedSample(indices, yorig, rest)
            xlist += (Xorig[sampleIdxs],)
            ylist += (yorig[sampleIdxs],)
        X = np.vstack(xlist)
        y = np.concatenate(ylist)
        return X,y
        
    def getSingleStratifiedSample(self, indices, y, sampleSize, bRegression = False):
        if bRegression:
            np.random.shuffle(indices)
            return indices[:sampleSize]
        if len(np.unique(y)) == 2:
            return self.getSingleStratifiedSampleBinary(indices, y, sampleSize)
        else:
            return self.getSingleStratifiedSampleMulticlass(indices, y, sampleSize)

    def getSingleStratifiedSampleMulticlass(self, indices, y, sampleSize):
        classes = np.unique(y)
        samples = {}
        overhead = 0
        for c in classes:
            idx = [i for i in indices if y[i] == c]       
            np.random.shuffle(indices)
            size = int(float(len(idx)) / float(len(indices)) * sampleSize)
            if overhead > 0 and size > 1:
                size -= 1                
            if size == 0:
                size = 1
                overhead += 1
            c_sample = indices[:size]
            samples[c] = c_sample             
        s = np.array([]).astype(int)
        for c in classes:
            s = np.append(s, samples[c])
        while len(s) < sampleSize:
            np.random.shuffle(indices)
            idx = indices[0]
            if idx not in s:
                s = np.append(s, [idx])
        s.astype(int)
        return s
  
    def getSingleStratifiedSampleBinary(self, indices, y, sampleSize):
        np.random.shuffle(indices)  
        pos = [i for i in indices if y[i] == 1.0]
        np.random.shuffle(indices)  
        neg = [i for i in indices if y[i] == 0.0]
        if len(pos)+len(neg) != len(indices):
            log( "Error: more classes than 1 and 0 for stratified binary sampling.")
        posSampleSize = int(float(len(pos)) / float(len(indices)) * sampleSize)
        negSampleSize = int(float(len(neg)) / float(len(indices)) * sampleSize)    
        if posSampleSize == 0:
            posSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if negSampleSize > 1:
                    negSampleSize -=1
        if negSampleSize == 0:
            negSampleSize = 1
            if posSampleSize + negSampleSize > sampleSize:
                if posSampleSize > 1:
                    posSampleSize -=1
        pos_samples = pos[:posSampleSize]
        neg_samples = neg[:negSampleSize]
        s = np.append(pos_samples,neg_samples)
        while len(s) < sampleSize:
            np.random.shuffle(indices)
            idx = indices[0]
            if idx not in s:
                s = np.append(s, [idx])
        s.astype(int)
        return s    
    
if __name__ == "__main__":
    data = Datasets()
    log( data.getAllDatasetNames())
    log( data.getTaskNames())
    log( data.getDatasetNames("classification"))
    X,y = data.getDataset("colic")
    log( X)
    log( y)